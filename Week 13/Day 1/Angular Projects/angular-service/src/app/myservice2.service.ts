import { Injectable } from "@angular/core";

@Injectable()                           // For this we can do DI
export class MyService2{
    dis1():string {
        return "Welcome to Angular Service with DI";
    }
}