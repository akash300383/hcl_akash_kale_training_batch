import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MyService2 } from './myservice2.service';
import { AppComponent } from './app.component';
import { SampleComponent } from './sample/sample.component';

@NgModule({
  declarations: [
    AppComponent,
    SampleComponent     // component details 
  ],
  imports: [
    BrowserModule
  ],
  providers: [MyService2],    // service details. 
  bootstrap: [AppComponent]
})
export class AppModule { }
