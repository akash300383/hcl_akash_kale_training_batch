import { Component, OnInit } from '@angular/core';
import { MyService1 } from '../myservice1.service';
import { MyService2 } from '../myservice2.service';
@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.css']
})
export class SampleComponent implements OnInit {
  msg1:string="";
  msg2:string ="";
  // singleton object created.
  constructor(public ser2:MyService2) { } // pull the object in constructor 
  ngOnInit(): void {
  }

  fun1() {
    let ser1 = new MyService1();
    this.msg1=ser1.dis1();
  }
  fun2() {
    this.msg2 =this.ser2.dis1();
  }
}
