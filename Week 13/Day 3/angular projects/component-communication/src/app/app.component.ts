import { Component, ViewChild } from '@angular/core';
import { Child2Component } from './child2/child2.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  parentName:string ="";
  cAge:number =0;
  childDesignation:string=""

  @ViewChild(Child2Component)     // reference of child2 component created in parent commponent
  child2Ref?:Child2Component;     //? optional property 

  setValue(pname:any){
    this.parentName=pname.value;
    if(this.child2Ref!=undefined){
      this.childDesignation=this.child2Ref?.designation;
    }
  }
  constructor(){
    
  }
}
