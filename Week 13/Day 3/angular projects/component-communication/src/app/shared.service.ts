import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  nums:Array<number>=[];
  constructor() { }

  setArrayValue(nums:Array<number>):void {
    this.nums=nums;
  }

  getArrayValue():Array<number> {
    return this.nums;
  }
  
}
