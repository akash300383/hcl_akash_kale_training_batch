import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
@Component({
  selector: 'app-child1',
  templateUrl: './child1.component.html',
  styleUrls: ['./child1.component.css']
})
export class Child1Component implements OnInit {
  @Input()            // it is ready to receive the value from parent component
  pName:string="";

  childAge:number =0;

  @Output()
  eventRef = new EventEmitter<number>();
  
  constructor() { }

  ngOnInit(): void {
  }

  setAge(ageRef:any){
    this.childAge = ageRef.value;
    this.eventRef.emit(this.childAge);    // pass the value 
  }
}
