import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
@Component({
  selector: 'app-child3',
  templateUrl: './child3.component.html',
  styleUrls: ['./child3.component.css']
})
export class Child3Component implements OnInit {

  nums:Array<number>=[];
  constructor(public ser:SharedService) { }   //DI for Service class

  ngOnInit(): void {
  }

  passValue(numRef:any){
        this.nums.push(numRef.value);
        numRef.value=""     // reset text field values;
        this.ser.setArrayValue(this.nums);          // set array value. 
  }

}
