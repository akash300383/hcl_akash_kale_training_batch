import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-child4',
  templateUrl: './child4.component.html',
  styleUrls: ['./child4.component.css']
})
export class Child4Component implements OnInit {
  nums:Array<number>=[];
  constructor(public ser:SharedService) { }   // DI for Service class

  ngOnInit(): void {
  }

  getValue() {
      this.nums=this.ser.getArrayValue();
  }
}
