import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  userName:string="";
  constructor(public router:Router) { // DI for Router  
      
  }
  ngOnInit(): void {
    //this.userName=sessionStorage.getItem("uname");
    let obj = sessionStorage.getItem("uname");
    if(obj!=null){
      this.userName=obj;
    }
  }
  logout() {
      sessionStorage.removeItem("uname");
      this.router.navigate(["login"]);
  }
}
