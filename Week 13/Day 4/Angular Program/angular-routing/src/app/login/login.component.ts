import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
loginRef = new FormGroup({
  user:new FormControl(),
  pass:new FormControl()
});
msg:string=""
  constructor(public router:Router) { }     // do DI for Router API 
  ngOnInit(): void {
  }
  checkUser() {
    let login = this.loginRef.value;
    // we write this code inside a subscribe method 
    if(login.user=="Raj" && login.pass=="123"){
        console.log("success");
        sessionStorage.setItem("uname",login.user);
        this.router.navigate(["home"]);
    }else {
        this.msg = "Invalid username or password"
        console.log("failure");
    }
    this.loginRef.reset();
  }
}
