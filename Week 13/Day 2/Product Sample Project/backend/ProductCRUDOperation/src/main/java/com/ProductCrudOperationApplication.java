package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "com")
@EntityScan(basePackages = "com.bean")
@EnableJpaRepositories(basePackages = "com.dao")
public class ProductCrudOperationApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductCrudOperationApplication.class, args);
		System.err.println("Server running on port number 9090");
	}

}
