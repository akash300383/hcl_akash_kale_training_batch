package com.sevice;

public interface AccountService {
	public String createAccount();
	public String withdraw(int accno, float amount);
	public String deposit(int accno,float amount);
	public float checkBalance(int acno);
}
