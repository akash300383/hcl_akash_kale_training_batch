package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Product;
import com.sevice.ProductSerivice;

@RestController
@RequestMapping("/product")
@CrossOrigin			/// this annotation is use enable cors policy 
public class ProductController {

	@Autowired
	ProductSerivice productService;
	
	@GetMapping(value = "allProducts",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Product> getAllProduct() {
		return productService.getAllProducts();
	}
	
	@PostMapping(value = "storeProduct",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeProduct(@RequestBody Product product) {
			return productService.storeProduct(product);
	}
	
	@PatchMapping(value = "updateProduct",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String updateProduct(@RequestBody Product product) {
		return productService.updateProductPrice(product);
	}
	
	@DeleteMapping(value = "deleteProduct/{pid}")
	public String updateProduct(@PathVariable("pid") int pid) {
		return productService.deleteProduct(pid);
	}
	
}
