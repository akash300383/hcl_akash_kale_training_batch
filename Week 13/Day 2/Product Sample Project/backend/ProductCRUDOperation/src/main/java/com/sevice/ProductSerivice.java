package com.sevice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Product;
import com.dao.ProductDao;

@Service
public class ProductSerivice {

	@Autowired
	ProductDao productDao;
	
				public List<Product> getAllProducts() {
						return productDao.findAll();
				}
				
				public String storeProduct(Product product) {
								if(productDao.existsById(product.getPid())) {
									return "Product id must be unqiue";
								}else {
										productDao.save(product);
										return "Record stored successfully";
								}
				}
					
				public String updateProductPrice(Product product) {
					if(productDao.existsById(product.getPid())) {
							Product pp =  productDao.getById(product.getPid());
							pp.setPrice(product.getPrice());
							productDao.saveAndFlush(pp);
							return "Product updated successfully";
					}else {
								return "Product not present";
					}
				}
				
				public String deleteProduct(int pid) {
					if(productDao.existsById(pid)) {
								productDao.deleteById(pid);
								return "Product deleted successfully";
					}else {
						return "Product not present";
					}

				}
}
