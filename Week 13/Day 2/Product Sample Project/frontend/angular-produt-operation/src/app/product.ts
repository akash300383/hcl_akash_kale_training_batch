// This class is use to map the json data retrieve from rest api
export class Product {
    constructor(
        public pid:number,
        public pname:string,
        public price:number,
        public url:string
    ){}
}
