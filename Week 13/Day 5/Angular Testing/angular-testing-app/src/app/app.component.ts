import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-testing-app';
  id:number =100;
  name:string ="Raj Deep";
  res:boolean = true;
  names:Array<string>=["Ravi","Ramesh","Rajesh"];


  checkUser(name:string,pass:string){
    if(name=="Raj" && pass=="123"){
      return "success"
    }else {
      return "failure";
    }
  }
}
