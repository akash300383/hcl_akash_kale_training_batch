import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {

  // hook 
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();   // it call automatically and load and compiled the component
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);  // component id will get
    const app = fixture.componentInstance;    //it will provide component reference. 
    expect(app).toBeTruthy();         // component created or not
  });

  it(`should have as title 'angular-testing-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('angular-testing-app'); // check the property value. 
  });


  it('propety testing', () => {
    const fixture = TestBed.createComponent(AppComponent);  // component id will get
    const app = fixture.componentInstance;    //it will provide component reference. 
    expect(app.id).toEqual(100);
    expect(app.name).toContain("Raj");
    expect(app.res).toBeTrue();
    expect(app.names.length).toBeGreaterThan(2);
  });

  it('function testing', () => {
    const fixture = TestBed.createComponent(AppComponent);  // component id will get
    const app = fixture.componentInstance;    //it will provide component reference. 
    let res = app.checkUser("Raj","123");
    expect(res).toEqual("success");
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();    // load the html or dom page 
    const compiled = fixture.nativeElement as HTMLElement;// we get dom reference. 
    expect(compiled.querySelector('.content span')?.textContent).toContain('running');
  });


});
