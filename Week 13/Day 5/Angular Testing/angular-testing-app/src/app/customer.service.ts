import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor() { }

  sayHello(name:string){
    return "Welcome "+name;
  }
}
