import { TestBed } from '@angular/core/testing';

import { CustomerService } from './customer.service';

describe('CustomerService', () => {
  let service: CustomerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CustomerService);  // di for service class 
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('service function testing ', () => {
    let result = service.sayHello("Raj");
    expect(result).toContain("Raj");
  });
});
