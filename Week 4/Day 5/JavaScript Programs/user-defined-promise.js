//ES5 style promise object creation 
var obj = new Promise(function(resolved,rejected){
    resolved("Done successfully...."),
    rejected("Error generated...")
});
// to load the result from promise 
obj.then(function(data){
    console.log("Then called "+data)
}).catch(function(error){
    console.log("Catch called "+error)
})
console.log("Finish")
console.log("Finish")
console.log("Finish")
// ES6 style promise object creation 
let obj1 = new Promise((resolved,rejected)=> {
    resolved("Done successfully...."),
    rejected("Error generated...")
});
obj1.then(data=>console.log(data)).catch(error=>console.log(error));
