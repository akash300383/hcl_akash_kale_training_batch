var a=10;
document.write("<br/>Value of a "+a)
a=20;   // re-initialization 
document.write("<br/>Value of a "+a)
var a="Ramesh"; // re-declaration 
document.write("<br/>Value of a "+a)
let b=10;
document.write("<br/> Value of b is "+b);
b=20;   // re-initializaiton 
document.write("<br/> Value of b is "+b);
//let b="Ramesh"; // re-declaration Error in this line 
document.write("<br/> Value of b is "+b);
// global scope using var 
for(var i=0;i<10000;i++){

}
document.write("<br/>Value of i declared inside loop "+i)
for(let j=0;j<10000;j++){

}
//document.write("<br/> Value of j is declred inside loop "+j)
const k=1000;
document.write("<br/> Value of k is "+k);
//k=2000;     // we can't change the value 
document.write("<br/> Value of k is "+k);

