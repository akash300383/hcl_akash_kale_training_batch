// ES5 style object creation using function 
// function Employee() {
//     this.id = 100;
//     this.name = "Raj";  // property 

//     this.dis = function() {         // behaviour 
//         document.write("<br/>id is "+this.id)
//         document.write("<br/>name is "+this.name)
//     }
// }
// var emp1 = new Employee();
// emp1.dis();
// var emp2 = new Employee();
// emp2.dis();
// parameterized constructor in ES5 style 
function Employee(id,name) {
    this.id = id;
    this.name = name;  // property 

    this.dis = function() {         // behaviour 
        document.write("<br/>id is "+this.id)
        document.write("<br/>name is "+this.name)
    }
}
var emp1 = new Employee(100,"Ravi");
emp1.dis();
var emp2 = new Employee(101,"Ramesh");
emp2.dis();