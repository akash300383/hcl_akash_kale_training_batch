package com.dao;

import java.util.List;

import com.bean.Employee;

public class EmployeeDao {

	public String getData() {
		// code to retrieve information from database. 
		// using jdbc or jpa or spring data 
		return "This information from dao method";
	}
	
	public List<Employee> getEmployeeFromDb() {
			// code or logic using jdbc or jpa to retrieve list of employee 
		return null;
	}
}
