package com.service;

import java.util.List;

import com.bean.Employee;
import com.dao.EmployeeDao;

public class EmployeeService {
	EmployeeDao ed = new EmployeeDao();
	public String getEmployeeInfo() {
		
		// business logic 
		return ed.getData();
	}
	
	
	public List<Employee> getEmployeeDetails() {
		
		return ed.getEmployeeFromDb();
	}
}
