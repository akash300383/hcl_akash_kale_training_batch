package com.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import org.mockito.junit.jupiter.MockitoExtension;

import com.bean.Employee;
import com.dao.EmployeeDao;
import com.service.EmployeeService;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceMockTest {
	@Mock
	EmployeeDao employeeDao;
	@InjectMocks
	EmployeeService employeeService;
	@Test
	@Disabled
	void testGetEmployeeInfo() {	
		// Fake Data for Dao layer 
		//Mockito.when(employeeDao.getData()).thenReturn("Fake Msg");
		
		// This code call actual dao method 
		Mockito.when(employeeDao.getData()).thenCallRealMethod();
		
		String res = employeeService.getEmployeeInfo();
		Assertions.assertEquals("Fake Msg", res);
		
	}
	
	@Test
	public void listOfEmployeeTest() {
		List<Employee> listOfEmp = new ArrayList<>();
		listOfEmp.add(new Employee(1, "Raj", 12000));
		listOfEmp.add(new Employee(2, "Ravi", 14000));
		// Fake record as list
		Mockito.when(employeeDao.getEmployeeFromDb()).thenReturn(listOfEmp);
		
		//Mockito.when(employeeDao.getEmployeeFromDb()).thenCallRealMethod();
		
		
		//Assertions.assertTrue(employeeService.getEmployeeDetails().stream().count()==2);
		Assertions.assertTrue(employeeService.getEmployeeDetails().get(0).getName().equals("Raj"));
		Assertions.assertTrue(employeeService.getEmployeeDetails().get(0).getSalary()==12000);
	}

}
