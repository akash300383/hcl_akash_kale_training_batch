package com.service;

import java.util.ArrayList;
import java.util.List;

import com.bean.Employee;

public class EmployeeService {

	public List<Employee> getAllEmployeeDetails() {
		List<Employee> listOfEmp = new ArrayList<>();
		listOfEmp.add(new Employee(1,"Ravi",12000));
		listOfEmp.add(new Employee(2,"Ramesh",14000));
		listOfEmp.add(new Employee(3,"Rajesh",18000));
		listOfEmp.add(new Employee(4,"Lokesh",20000));
		return listOfEmp;
	}
	
	public Employee getEmployee() {
		Employee emp1 = new Employee(1,"Ajay",22000);
		return emp1;
		
	}
}
