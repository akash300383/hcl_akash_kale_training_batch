package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.service.Operation;

@DisplayName("Operation Testing")
class OperationTest {

	@Test
	@DisplayName("Addition Testing")
	void testAdd() {
		Operation op = new Operation();
		int res = op.add(10, 20);
		Assertions.assertEquals(30, res);
	}

	@Test
	@DisplayName("Substraction Testing")
	@Disabled
	void testSub() {
		//fail("Not yet implemented");
		Operation op = new Operation();
		int res = op.sub(100, 50);
		Assertions.assertEquals(50, res);
	}

}
