package com.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.bean.Employee;
import com.service.EmployeeService;

class EmployeeServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	
	void testGetAllEmployeeDetails() {
		//fail("Not yet implemented");
		//Assertions.assertTrue(new EmployeeService().getAllEmployeeDetails().stream().count()==4);
		//Assertions.assertEquals(new EmployeeService().getAllEmployeeDetails().stream().filter(e->e.getSalary()>15000).count(), 3);
		//Assertions.assertEquals(new EmployeeService().getAllEmployeeDetails().stream().filter(e->e.getSalary()>15000).findAny().isPresent(), true);
		Assertions.assertEquals(new EmployeeService().getAllEmployeeDetails().stream().allMatch(e->e.getSalary()>10000), true);
	}

	@Test
	void testGetEmployee() {
		//fail("Not yet implemented");
//		EmployeeService es = new EmployeeService();
//		Employee emp = es.getEmployee();
//		Assertions.assertTrue(emp.getSalary()>15000, "Salary must be greater than 15000");
		
		Assertions.assertTrue(new EmployeeService().getEmployee().getSalary()>15000, "Salary must be > 15000");
	}

}
