package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("Testing with hook method")
class JUnitHookMethods {
	@BeforeAll				// @BeforeClass 
	static void setUpBeforeClass() throws Exception {
		System.out.println("This method call only once before all test method");
	}
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		System.out.println("This method call only once after call test method");
	}
	@BeforeEach			// @Before 
	void setUp() throws Exception {
		System.out.println("This method call before each test method");
	}
	@AfterEach
	void tearDown() throws Exception {
		System.out.println("This method called after each test method");
	}
	@Test
	@DisplayName("Testing assert true ")
	void test1() {
		Assertions.assertTrue(true);       
		System.out.println("While testing true assert method");
	}
	@Test
	@DisplayName("Testring assert false")
	void test2() {
		Assertions.assertFalse(false);
		System.out.println("While testing false assert method");
	}
}
