package com.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SimpleController {

	private static final Logger logger = LogManager.getLogger(SimpleController.class);
	
	@GetMapping(value = "say")
	public String sayHello() {
		//System.out.println(" I came here");
		logger.info("I came Here");
		logger.error("Exception generated");
		logger.warn("This is warning message");
		return "Welcome to Spring boot with logger API";
	}
}
