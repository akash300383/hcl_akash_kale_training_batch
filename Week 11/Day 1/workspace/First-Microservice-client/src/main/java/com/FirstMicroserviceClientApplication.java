package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication(scanBasePackages = "com")
@EnableEurekaClient
public class FirstMicroserviceClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstMicroserviceClientApplication.class, args);
		System.err.println("First Client running on port number 8181");
	}

}
