package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Paytm;
import com.dao.PaytmDao;

@Service
public class PaytmService {

	@Autowired
	PaytmDao paytmDao;
	
	public String paytmAccountCreate(Paytm pp) {
		if(paytmDao.existsById(pp.getPid())) {
			return "Paytm account id must be unique";
		}else {
			paytmDao.save(pp);
			return "Account Created successfully";
		}
	}
	
	public int getAccountNumber(int pid) {
		if(paytmDao.existsById(pid)) {
			Paytm pt = paytmDao.getById(pid);
			return pt.getAccno();
		}else {
			return 0;
		}
	}
	
}
