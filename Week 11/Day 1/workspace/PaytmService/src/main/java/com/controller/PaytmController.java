package com.controller;

import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.bean.Paytm;
import com.service.PaytmService;

@RestController
@RequestMapping("paytm")
public class PaytmController {

	@Autowired
	PaytmService paytmService;
	
	@Autowired
	RestTemplate restTemplate;				// pull the object from restTemplate method(user defined method)
	
	@PostMapping(value = "create")
	public String paytmAccountCreate(@RequestBody Paytm pp) {
		return paytmService.paytmAccountCreate(pp);
	}
	
	@GetMapping(value="checkBalance/{pid}")
	public String checkBalance(@PathVariable("pid") int pid) {
		int accno = paytmService.getAccountNumber(pid);
		if(accno==0) {
			return "Account number not link with paytm account or paytm id is wrong";
		}else {
			//restTemplate.for
ResponseEntity<String>  res = restTemplate.getForEntity("http://account-service:8282/account/checkBalance/"+accno, String.class);
	return res.getBody();
		}
		
	}
}
