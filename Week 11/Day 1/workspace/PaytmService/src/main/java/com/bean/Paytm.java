package com.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Paytm {
@Id
private int pid;
private String pname;
private int accno;
public int getPid() {
	return pid;
}
public void setPid(int pid) {
	this.pid = pid;
}
public String getPname() {
	return pname;
}
public void setPname(String pname) {
	this.pname = pname;
}
public int getAccno() {
	return accno;
}
public void setAccno(int accno) {
	this.accno = accno;
}
@Override
public String toString() {
	return "Paytm [pid=" + pid + ", pname=" + pname + ", accno=" + accno + "]";
}

}
