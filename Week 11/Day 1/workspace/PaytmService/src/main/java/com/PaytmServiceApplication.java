package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication(scanBasePackages = "com")
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.dao")
@EntityScan(basePackages = "com.bean")
public class PaytmServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaytmServiceApplication.class, args);
		System.err.println("Paytm service running on port number 8383");
	}

	@Bean					// object created by us 
	@LoadBalanced		// scale the object creation calling the service using logical name rather than physical id address. 
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
