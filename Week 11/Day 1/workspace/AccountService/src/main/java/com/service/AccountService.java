package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Account;
import com.dao.AccountRepository;

@Service
public class AccountService {

	@Autowired
	AccountRepository accountRepository;
	

		public String createAccount(Account acc) {
			accountRepository.save(acc);
			return "Account Created successfully";
		}
		
		public String checkBalance(int accno) {
			if(accountRepository.existsById(accno)) {
				Account acc = accountRepository.getById(accno);
				return "Your Balance is "+acc.getAmount();
			}else {
				return "InValid Account number";
			}
		}
		public String deposit(Account acc) {
			if(accountRepository.existsById(acc.getAccno())) {
				Account account = accountRepository.getById(acc.getAccno());
				account.setAmount(account.getAmount()+acc.getAmount());
				accountRepository.saveAndFlush(account);
				return "Amount deposit successfully";
			}else {
				return "Account number not present";
			}
		}
		public String withdraw(Account acc) {
			if(accountRepository.existsById(acc.getAccno())) {
				Account account = accountRepository.getById(acc.getAccno());
				account.setAmount(account.getAmount()-acc.getAmount());
				accountRepository.saveAndFlush(account);
				return "Amount withdraw successfully";
			}else {
				return "Account number not present";
			}
		}
}
