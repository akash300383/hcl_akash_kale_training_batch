package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Account;
import com.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	AccountService accountService;
	
	@PostMapping(value = "create",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String createAccount(@RequestBody Account acc) {
		return accountService.createAccount(acc);
	}
	
	@GetMapping(value = "checkBalance/{accno}")
	public String checkBalance(@PathVariable("accno") int accno) {
		return accountService.checkBalance(accno);
	}
	
	@PatchMapping(value = "deposit")
	public String depositAmount(@RequestBody Account acc) {
		return accountService.deposit(acc);
	}

	@PatchMapping(value = "withdraw")
	public String withdrawAmount(@RequestBody Account acc) {
		return accountService.withdraw(acc);
	}
}
