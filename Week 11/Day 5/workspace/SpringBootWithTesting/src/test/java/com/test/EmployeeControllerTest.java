package com.test;

import static org.junit.jupiter.api.Assertions.*;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.client.RestTemplate;

import com.bean.Employee;
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)		// for controller. 
class EmployeeControllerTest {
	
	String baseUrl = "http://localhost:8080/employee";
	
	@Test
	@Disabled
	void testGetEmployeeInfo() {
		//fail("Not yet implemented");
		RestTemplate restTemplate = new RestTemplate();	
		Employee emp = restTemplate.getForObject(baseUrl+"/findById/2", Employee.class);
		assertEquals("Ajay", emp.getName());
		assertEquals(16000, emp.getSalary());
		assertEquals(2, emp.getId());
	}
	@Test
	void testStoreEmployee() {
		RestTemplate restTemplate = new RestTemplate();
		Employee emp = new Employee();
		emp.setId(10);
		emp.setName("Raj");
		emp.setSalary(10000);
		String res	= restTemplate.postForObject(baseUrl+"/storeEmployee", emp, String.class);
		assertEquals(res, "Employee id must be unique");
	}

}
