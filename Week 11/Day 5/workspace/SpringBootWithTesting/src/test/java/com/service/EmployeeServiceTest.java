package com.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bean.Employee;

@SpringBootTest
class EmployeeServiceTest {

	@Autowired
	EmployeeService employeeService;				// normal service class test 
	
	@Test
	void testGetEmployee() {
	Employee emp	= employeeService.getEmployee(2);
	Assertions.assertEquals("Ajay", emp.getName());
	}

	@Test
	@Disabled
	void testStoreEmployeeInfo() {
		fail("Not yet implemented");
	}

}
