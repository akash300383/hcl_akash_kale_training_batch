package com.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Employee;
import com.dao.EmployeeDao;

@Service
public class EmployeeService {
	@Autowired
	EmployeeDao employeeDao;
	public Employee getEmployee(int id) {
			if(employeeDao.existsById(id)) {
			Optional<Employee> op	=  employeeDao.findById(id);
			
			if(op.isPresent()) {
				return op.get();
			}else {
				return null;
			}
			
			}else {
				return null;
			}
	}
	public String storeEmployeeInfo(Employee emp) {
		if(employeeDao.existsById(emp.getId())) {
			return "Employee id must be unique";
		}else {
			employeeDao.save(emp);
			return "Record stored successfully";
		}
	}
}
