package com;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class SessionDemo
 */
/* <servlet></servlet> <servlet-mapping></servlet-mapping>*/
@WebServlet("/SessionDemo")	// url pattern name
public class SessionDemo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SessionDemo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    int count=0;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter pw = response.getWriter();
		pw.println("<font size=5>");
		// This code provide the session object in Servlet.
		response.setContentType("text/html");
		HttpSession hs = request.getSession();
		pw.println("Count value is "+count);
		count++;
		if(hs.isNew()) {
			pw.println("<br/> New Client");
		}else {
			pw.println("<br/> Old Client");
		}
		pw.println("<br/> Session Id "+hs.getId());
		pw.println("<br> Sesison Creation time "+new Date(hs.getCreationTime()));
		pw.println("<br> last Access Creation time "+new Date(hs.getLastAccessedTime()));
		pw.println("<br> Default time for sesssion "+hs.getMaxInactiveInterval());		// 1800
		hs.setMaxInactiveInterval(600);
		if(count%5==0) {
			hs.invalidate();     // destroy the session 
		}
		pw.println("</font>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
