package com.dao;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import com.bean.Employee;
public class EmployeeDao {

	public int storeEmployeeRecord(Employee emp) {
		try {
		Configuration con = new Configuration();
		con.configure("hibernate.cfg.xml");				// loading the xml file which load database connectivity details with entity class
		SessionFactory sf = con.buildSessionFactory();		// SessionFactory is like a Connection in JDBC.
		Session session = sf.openSession();					// Session is like a Statement or PreparedStatement 
		Transaction tran = session.getTransaction();
		tran.begin();
		session.save(emp);						// storing the object directly in database. 
		tran.commit();
		return 1;
		}catch(Exception e) {
				System.out.println(e);
				return 0;
		}
	
	}
	
	// update salary or name using id 
	public int updateEmployeeRecord(Employee emp) {
		Configuration con = new Configuration();
		con.configure("hibernate.cfg.xml");				// loading the xml file which load database connectivity details with entity class
		SessionFactory sf = con.buildSessionFactory();		// SessionFactory is like a Connection in JDBC.
		Session session = sf.openSession();					// Session is like a Statement or PreparedStatement 
		Employee e	=session.get(Employee.class, emp.getId());			// in get method 1st parameter class reference 2nd parameter column pk
		Transaction tran = session.getTransaction();
		if(e==null) {
			return 0;
		}else {
				tran.begin();
				e.setSalary(emp.getSalary());      // replace old salary by new salary 
				session.update(e);						// update existing entity 
				tran.commit();
				return 1;
		}
	}
		
		// delete employee record base upon employee id 
		public int deleteEmployeeRecord(int empId) {
			Configuration con = new Configuration();
			con.configure("hibernate.cfg.xml");				// loading the xml file which load database connectivity details with entity class
			SessionFactory sf = con.buildSessionFactory();		// SessionFactory is like a Connection in JDBC.
			Session session = sf.openSession();					// Session is like a Statement or PreparedStatement 
			Employee e	=session.get(Employee.class, empId);			// in get method 1st parameter class reference 2nd parameter column pk
			Transaction tran = session.getTransaction();
			if(e==null) {
				return 0;
			}else {
					tran.begin();
						session.delete(e);
					tran.commit();
					return 1;
			}
	}
	
		// Retrieve the Receords base upon the Id 
			public Employee findRecord(int empId) {
			Configuration con = new Configuration();
			con.configure("hibernate.cfg.xml");				// loading the xml file which load database connectivity details with entity class
			SessionFactory sf = con.buildSessionFactory();		// SessionFactory is like a Connection in JDBC.
			Session session = sf.openSession();					// Session is like a Statement or PreparedStatement 
			Employee e 	=session.get(Employee.class, empId);			// in get method 1st parameter class reference 2nd parameter column pk
			return e;
	}
	// Retrieve all employee records 
			public List<Employee> getAllEmployees() {
					Configuration con = new Configuration();
					con.configure("hibernate.cfg.xml");				// loading the xml file which load database connectivity details with entity class
					SessionFactory sf = con.buildSessionFactory();		// SessionFactory is like a Connection in JDBC.
					Session session = sf.openSession();					// Session is like a Statement or PreparedStatement 
					Query qry = session.createQuery("select emp from Employee emp");		
					List<Employee> listOfEmp = qry.list();
					return listOfEmp;
			}
			
		public List<Employee> getAllEmployeesWithCondition(float salary) {
				Configuration con = new Configuration();
				con.configure("hibernate.cfg.xml");				// loading the xml file which load database connectivity details with entity class
				SessionFactory sf = con.buildSessionFactory();		// SessionFactory is like a Connection in JDBC.
				Session session = sf.openSession();					// Session is like a Statement or PreparedStatement 
				//Query qry = session.createQuery("select emp from Employee emp where emp.salary > 18000");	
				// label query 
				Query qry = session.createQuery("select emp from Employee emp where emp.salary > :sal");
				qry.setParameter("sal", salary);
			//	Query qry = session.createQuery("select emp from Employee emp where emp.name like 'Ravi' ");
				
				List<Employee> listOfEmp = qry.list();
				return listOfEmp;
		}
}
