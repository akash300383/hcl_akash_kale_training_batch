package com.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Product;
import com.service.ProductService;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	ProductService productService;
	
	@GetMapping(value = "getAllProduct",
	produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Product> getAllProductInfo() {
		return productService.getAllProducts();
	}
	@PostMapping(value = "storeProduct",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeProductInfo(@RequestBody Product prod) {
		
				return productService.storeProductInfo(prod);
	}
	
	@DeleteMapping(value = "deleteProduct/{pid}")
	public String storeProductInfo(@PathVariable("pid") int pid) {
					return productService.deleteProductInfo(pid);
	}
	
	@PatchMapping(value = "updateProduct")
	public String updateProductInfo(@RequestBody Product prod) {
					return productService.updateProductInfo(prod);
	}
	
	@GetMapping(value = "findProdutByPrice/{price}")
	public List<Product> findProductByPrice(@PathVariable("price") float price) {
		return productService.findProductUsingPrice(price);
	}
}



