package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Product;
import com.dao.ProductDao;

@Service
public class ProductService {

	@Autowired
	ProductDao productDao;
	
	public String storeProductInfo(Product pro) {
		if(pro.getPrice()<1000) {
			return "Product price must be > 10000";
		}else {
			if(productDao.storeProductInfo(pro)>0) {
				return "Product stored successfully";
			}else {
				return "Product didn't store";
			}
		}
	}
	
	public List<Product> getAllProducts() {
		return productDao.getAllProduct();
	}
	
	
}
