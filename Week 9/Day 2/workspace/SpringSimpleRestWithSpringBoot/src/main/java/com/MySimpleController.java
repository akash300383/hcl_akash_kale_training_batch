package com;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MySimpleController {

	@RequestMapping(value = "sayHello")
	public String sayHello() {
		return "Welcome to Simple Spring boot with Rest API";
	}
	@RequestMapping(value = "sayHtml",method = RequestMethod.GET,produces = MediaType.TEXT_HTML_VALUE)
	public String sayHtml() {
		return "<h2>Welcome to Simple Rest API</h2>";
	}
	@RequestMapping(value = "sayXml",method = RequestMethod.GET,produces = MediaType.TEXT_XML_VALUE)
	public String sayXml() {
		return "<h2>Welcome to Simple Rest API</h2>";
	}
	@RequestMapping(value = "sayText",method = RequestMethod.GET,produces = MediaType.TEXT_PLAIN_VALUE)
	public String sayText() {
		return "<h2>Welcome to Simple Rest API</h2>";
	}
	// Query param and path Param 
	
	@RequestMapping(value = "empInfo",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public Employee getEmployee() {
		Employee emp = new Employee();
		emp.setId(100);
		emp.setName("Raju");
		emp.setSalary(12000);
		return emp;
	}
	
	
	@RequestMapping(value = "allEmpInfo",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Employee> getAllEmployee() {
		Employee emp1 = new Employee(100,"Raj",12000);
		Employee emp2 = new Employee(101,"Ravi",14000);
		Employee emp3 = new Employee(102,"Ramesh",16000);
		Employee emp4 = new Employee(103,"Raju",18000);
		List<Employee> listOfEmp = new ArrayList<>();
		listOfEmp.add(emp1);
		listOfEmp.add(emp2);
		listOfEmp.add(emp3);
		listOfEmp.add(emp4);
		return listOfEmp;
	}
	
	@RequestMapping(value = "storeEmployee",
			method=RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeEmployeeInfo(@RequestBody Employee emp) {
		System.out.println(emp);
		return "Record stored";
	}
	
	@RequestMapping(value = "updateEmployee",
			method=RequestMethod.PATCH,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String updatetEmployeeInfo(@RequestBody Employee emp) {
		System.out.println(emp);
		return "Record updated";
	}

	@RequestMapping(value = "deleteEmployee/{id}",
			method=RequestMethod.DELETE)
	public String deleteEmployeeInfo(@PathVariable("id") int id) {
		return "Record deleted with id as "+id;
	}
	
}
