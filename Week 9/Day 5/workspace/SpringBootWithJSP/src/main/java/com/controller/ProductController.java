package com.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.bean.Product;
import com.service.ProductService;

@Controller
public class ProductController {

	
	@Autowired
	ProductService productService;
	
	@GetMapping(value = "displayProducts")
	public String showAllProducts(HttpSession hs) {// DI for HttpSession, HttpServletRequest, HttpServletREsponse 
		List<Product> listOfProducts = productService.getAllProduct();
		hs.setAttribute("products", listOfProducts);
		return "displayProduct";
	}
}
