package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bean.Employee;


@Repository
public class EmployeeDao {

	
	@Autowired
	SessionFactory sessionFactory;
	
	public List<Employee> getAllEmployeeDetails() {
		Session session = sessionFactory.openSession();
		Query<Employee> qry = session.createQuery("select emp from Employee emp");
		List<Employee> listOfEmp = qry.list();
		return listOfEmp;
	}
	
	public int deleteEmployeeUsingId(int id) {
		Session session = sessionFactory.openSession();
		Transaction tran = session.getTransaction();
		Employee emp = session.get(Employee.class, id);
		if(emp==null) {
				return 0;
		}else {
			tran.begin();
				session.delete(emp);
			tran.commit();
			return 1;
		}
		
	}
		public int updateEmployeeSalary(Employee e) {
			Session session = sessionFactory.openSession();
			Transaction tran = session.getTransaction();
			Employee emp = session.get(Employee.class, e.getId());
			if(emp==null) {
					return 0;
			}else {
				tran.begin();
					emp.setSalary(e.getSalary());
					session.update(emp);
				tran.commit();
				return 1;
			}
	}
	
		
		public int storeEmployee(Employee emp) {
			try {
			Session session = sessionFactory.openSession();
			Transaction tran = session.getTransaction();
			tran.begin();
				session.save(emp);
			tran.commit();
			return 1;
			}catch(Exception e) {
				return 0;
			}
	}
}
