package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Employee;
import com.dao.EmployeeDao;

@Service
public class EmployeeService {

	@Autowired
	EmployeeDao employeeDao;
	
	public List<Employee> getAllEmployee() {
		return employeeDao.getAllEmployeeDetails();
	}
	
	public String deleteRecord(int id) {
		if(employeeDao.deleteEmployeeUsingId(id)>0) {
			return "Record deleted successfully";
		}else {
			return "Record didn't delete";
		}
	}
	
	public String upateRecord(Employee emp) {
		if(employeeDao.updateEmployeeSalary(emp)>0) {
			return "Record updated successfully";
		}else {
			return "Record didn't update";
		}
	}
	
	public String storeEmployeeInfo(Employee emp) {
		if(employeeDao.storeEmployee(emp)>0) {
			return "Record stored successfully";
		}else {
			return "Record didn't store";
		}
	}
	
}
