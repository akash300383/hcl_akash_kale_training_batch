package com;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

	public static void main(String[] args) {

		// using XML file with annotation 
//ApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
//Employee emp = (Employee)ac.getBean("employee");
//System.out.println(emp);
		
		// with configuration class with annotation 
		ApplicationContext ac = new AnnotationConfigApplicationContext(MyConfiguration.class);
		Employee emp = (Employee)ac.getBean("employee");
		System.out.println(emp);
	}

}
