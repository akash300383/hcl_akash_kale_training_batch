package com.controller;



import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.bean.Product;
import com.service.ProductService;



@Controller
//@RequestMapping("/product")
public class ProductController {

	@Autowired
		ProductService productService;
	
		@GetMapping(value = "display")
		public ModelAndView getAllProduct(HttpServletRequest req) {
			ModelAndView mav = new ModelAndView();
			List<Product> listOfProduct = productService.getAllProduct();
			req.setAttribute("products", listOfProduct);
			req.setAttribute("msg", "All Product Details");
			mav.setViewName("display.jsp");
			return mav;
		}
		
		@GetMapping(value = "like")
		public ModelAndView likeProduct(HttpServletRequest req) {
			ModelAndView mav = new ModelAndView();
			int pid = Integer.parseInt(req.getParameter("pid"));
			System.out.println("Product id is "+pid);
			List<Product> listOfProduct = productService.getAllProduct();
			req.setAttribute("products", listOfProduct);
			req.setAttribute("msg", "All Product Details");
			mav.setViewName("display.jsp");
			return mav;
		}
}
