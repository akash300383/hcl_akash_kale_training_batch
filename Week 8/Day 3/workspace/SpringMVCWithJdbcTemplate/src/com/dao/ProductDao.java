package com.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.bean.Product;
import com.mysql.cj.Query;

@Repository
public class ProductDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public List<Product> getAllProduct() {
		return jdbcTemplate.query("select * from product", new ProductRowMapper());
	}
	
	
	public List<Product> findProductById(int pid) {
	//jdbcTemplate.query("select * from login where username like ? and password like ?", new Object[] {user,pass},new ProductRowMapper())
		return jdbcTemplate.query("select * from product where pid=?",new Object[] {1},new ProductRowMapper());
	}
}
class ProductRowMapper implements RowMapper<Product>{
	@Override
	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Product p = new Product();
		p.setPid(rs.getInt(1));
		p.setPname(rs.getString(2));
		p.setPrice(rs.getFloat(3));
		p.setUrl(rs.getString(4));
		return p;
	}
	
	
}