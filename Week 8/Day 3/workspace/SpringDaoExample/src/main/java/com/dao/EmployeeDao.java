package com.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import com.bean.Employee;

@Repository
public class EmployeeDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	public int storeEmployeeInfo(Employee emp) {
		try {
			// same method for insert , delete and update 
			return jdbcTemplate.update("insert into employee values(?,?,?)", emp.getId(),emp.getName(),emp.getSalary());
		} catch (Exception e) {
			System.out.println(e);
			return 0;
		}
	}
	
	
	public List<Employee> retrieveEmployeeInfo() {
		try {
			// Before Java8
			//return jdbcTemplate.query("select * from employee", new MyRowMapper());
			
			// After Java 8
			return jdbcTemplate.query("select * from employee", (rs,rowNum)-> {
				Employee emp =new Employee();
				emp.setId(rs.getInt(1));
				emp.setName(rs.getString(2));
				emp.setSalary(rs.getFloat(3));
				return emp;
			});
		} catch (Exception e) {
			System.out.println(e);
		}
		return null;
	}
}
// This class is reponsible to convert query into objects. 
class MyRowMapper implements RowMapper<Employee>{
	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		Employee emp =new Employee();
		emp.setId(rs.getInt(1));
		emp.setName(rs.getString(2));
		emp.setSalary(rs.getFloat(3));
		return emp;
	}
	
}


