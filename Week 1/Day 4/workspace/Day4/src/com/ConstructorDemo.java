package com;
class Demo {
		Demo() {
			System.out.println("This is constructor");
		}
		void displayInfo() {
			System.out.println("This is simple method");
		}
}
public class ConstructorDemo {
	public static void main(String[] args) {
	Demo obj = new Demo();
	obj.displayInfo();
	obj.displayInfo();
	obj.displayInfo();
	Demo obj1  = new Demo();
	obj1.displayInfo();
	obj1.displayInfo();
	}
}
