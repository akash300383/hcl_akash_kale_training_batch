package abc;

public class EncapsulationDemo {

	public static void main(String[] args) {
		Employee emp = new Employee();
		//emp.name="Raj";
		//emp.salary=-12000;
		emp.setValue("Raj", -12000);
		emp.display();
		
		Customer c1 = new Customer();
		c1.setCid(100);
		c1.setCname("Raj");
		c1.setAge(21);
		
		System.out.println("Id is "+c1.getCid());
		System.out.println("Name is "+c1.getCname());
		System.out.println("Age is "+c1.getAge());
		
		Customer c2 = new Customer(101, "Ramesh", 22);
		System.out.println("Id is "+c2.getCid());
		System.out.println("Name is "+c2.getCname());
		System.out.println("Age is "+c2.getAge());
		
		Customer c3 = new Customer(102, "Ajay", 24);
		System.out.println(c3);			// toString() 
	}

}
