package mno;
final class A {
		final void dis1() {
			System.out.println("This is A class method");
		}
}
//class B extends A{
////	void dis1() {
////		System.out.println("This is B class method - Override");
////	}
//}
public class FinalDemo {

	public static void main(String[] args) {
	final int A=10;
	System.out.println(A);
	//A=20;		can't change the value 
	}

}
