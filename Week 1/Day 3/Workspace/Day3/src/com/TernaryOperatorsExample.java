package com;

public class TernaryOperatorsExample {

	public static void main(String[] args) {
		int a=10;
		int b=50;
		String result;
		boolean res;
		// syntax 
		// condition?trueblock:falseblock;
		result = a>b?" a is largest ":" b is largest ";
		res = a>b?true:false;
		System.out.println(result);
		System.out.println(res);
	}

}
