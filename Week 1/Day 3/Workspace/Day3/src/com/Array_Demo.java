package com;

public class Array_Demo {

	public static void main(String[] args) {
		int abc[];			// array declaration 
		int num[]= {10,20,30,40,50,60};		// array declaration with initialilzation 
		System.out.println("0 position "+num[0]);
		System.out.println("1 position "+num[1]);
		System.out.println("using for loop");
		for(int i=2;i<4;i++) {
			System.out.println(" Value is "+num[i]);
		}
		System.out.println("using ehanced for loop");
		for(int n : num) {
			System.out.println("Value is "+n);
		}
	}

}
