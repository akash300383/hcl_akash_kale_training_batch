package com;
import java.util.Scanner;
public class SwitchStatement {

	public static void main(String[] args) {
		int ch=10;
		switch(ch) {
		case 1: System.out.println("1st block");
					break;
		case 2: System.out.println("2nd block");
					break;
		case 3: System.out.println("3rd block");
					break;
		case 4: System.out.println("4th block");
				break;
		default : System.out.println("Wrong choice");
		         break;
		}
		System.out.println("Finish");
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the name");
		String abc=sc.next();
		switch(abc) {
		case "a": System.out.println("a block");
					break;
		case "b": System.out.println("b block");
					break;
		case "c": System.out.println("c block");
					break;
		case "d": System.out.println("d block");
				break;
		default : System.out.println("Wrong choice");
		         break;
		}
		System.out.println("Finish");
	}
}
