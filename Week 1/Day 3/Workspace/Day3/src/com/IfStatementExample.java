package com;

public class IfStatementExample {
	public static void main(String[] args) {
		int a=10;
		int b=50;
		if(a>b) {
			System.out.println("a>b");
		}else {
			System.out.println("b>a");
		}
		System.out.println("Finish");
	}
}
