package abc;
class A {
		A() {
			System.out.println("A class empty constructor");
		}
		A(int x){
			this();
			System.out.println(" A class parameterized constructor");
		}
}
class B extends A {
	B(){
		super(10);			// it is use to call super class empty constructor : by default present. 
		System.out.println(" B class empty constructor");
	}
}
public class SuperParameterDemo {

	public static void main(String[] args) {
		B obj = new B();
	}

}
