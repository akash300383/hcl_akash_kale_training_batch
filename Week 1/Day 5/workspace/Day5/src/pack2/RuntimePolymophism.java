package pack2;
//class A {
//	void dis1() {
//		System.out.println(" A class dis1 method");
//	}
//}
//abstract class A {
//	abstract void dis1();
//}
//class D extends A {
//		void dis1() {
//			System.out.println(" D class override dis1() method");
//		}
//}
interface A {
	void dis1();
}
class D implements A {
		public void dis1() {
			System.out.println(" D class override dis1() method");
		}
}
public class RuntimePolymophism {
	public static void main(String[] args) {
		//A obj1  = new A();			// super class object 
		//D obj2 = new D();			// sub class object 
		//D obj3 = new A();			// super class object and sub reference not possible. 
		A obj4  = new D();			// sub class object and super class reference possible. run time polymoprhism 
												// super class can be normal class or abstract class or interface. 
	}

}
