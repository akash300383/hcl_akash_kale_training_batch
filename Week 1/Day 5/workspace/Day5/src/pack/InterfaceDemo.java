package pack;
interface A {
	int M=10;
	int X=100;
	void dis1();
	void dis();
}
interface B {
	int N=20;
	int X=200;
	void dis2();
	void dis();
}
interface C extends A,B{
	int O=30;
	void dis3();
}
class D implements A,B {
		public void dis() {
			System.out.println(" A and B interface method");
			System.out.println(M);
			System.out.println(N);
			System.out.println(A.X);
			System.out.println(B.X);
		}
		public void dis1() {
				System.out.println("A interface method");
		}
		public void dis2() {
			System.out.println(" B interface method");
		}
}
public class InterfaceDemo {
	public static void main(String[] args) {
		D obj = new D();
		obj.dis1();
		obj.dis2();
		obj.dis();
	}

}
