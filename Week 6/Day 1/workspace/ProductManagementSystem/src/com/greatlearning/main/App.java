package com.greatlearning.main;

import java.util.Scanner;

import com.greatlearning.bean.Product;
import com.greatlearning.service.ProductService;

public class App {

	public static void main(String[] args) {
		Scanner obj = new Scanner(System.in);
		ProductService ps = new ProductService();
		int choice;
		int pid;
		String pname;
		float price;
		String con;
		do {
			System.out.println("1: Add Product 2: Display Product 3:FindProduct 4:Delete Product 5: Update Price");
			System.out.println("Enter your choice");
			choice = obj.nextInt();
			
			
			switch(choice) {
			case 1:  Product pp = new Product(); 
						System.out.println("Enter the product Id");
			            pid = obj.nextInt();
			            pp.setPid(pid);
			            System.out.println("Enter the product name");
			            pname = obj.next();
			            pp.setPname(pname);
			            System.out.println("Enter the price");
			            price = obj.nextFloat();
			            pp.setPrice(price);
			           // Product pp = new Product(pid, pname, price);
			            System.out.println(ps.addProduct(pp));
						break;
			case 2: ps.getAllProductDetails().forEach(p->System.out.println(p));
						break;
			case 3:System.out.println("Plz enter the product id");
			           pid = obj.nextInt();
						Product p  = ps.findProductById(pid);
			           if(p==null) {
							System.out.println("Product not present");
						}else {
							System.out.println(p);
						}
						break;
			case 4:System.out.println("Plz enter the product id to delete");
			          pid = obj.nextInt();
			          String deleteMsg  = ps.deleteProductInfo(pid);
			          System.out.println(deleteMsg);
			          break;
			case 5:System.out.println("Enter the product id");
			   			pid = obj.nextInt();
			   			System.out.println("Enter the product price");
			   			price = obj.nextFloat();
			   			Product updateProduct = new Product();
			   			updateProduct.setPid(pid);
			   			updateProduct.setPrice(price);
			   			String updateMsg  = ps.updateProductInfo(updateProduct);
			   			System.out.println(updateMsg);
			   			break;         
			default:System.out.println("Wrong choice");
						break;
			}
			System.out.println("Do you want to continue?");
			con = obj.next();
		} while (con.equalsIgnoreCase("y"));
		System.out.println("Thank You!");
	}

}
