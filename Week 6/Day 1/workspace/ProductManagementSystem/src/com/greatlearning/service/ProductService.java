package com.greatlearning.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import com.greatlearning.bean.Product;

public class ProductService {

		List<Product> listOfProduct = new ArrayList<>();
		Map<Integer, Product> listOfProductDetails = new HashMap<>();
		public String addProduct(Product pp) {
				if(listOfProduct.size()==0) {
					listOfProduct.add(pp);
					return "Product Added successfully";
				}else if(listOfProduct.stream().filter(p->p.getPid()==pp.getPid()).count()>0){
					return "Product id must be unique";
				}else {
					listOfProduct.add(pp);
					return "Product Added successfully";
				}
				
		}
		
		public List<Product> getAllProductDetails() {
			return listOfProduct;
		}
		
		public Product findProductById(int pid) {
		Optional<Product> op = listOfProduct.stream().filter(p->p.getPid()==pid).findFirst();
		if(op.isPresent()){
			return op.get();
		}else {
			return null;
		}
//			List<Product> productInfo = listOfProduct.stream().filter(p->p.getPid()==pid).collect(Collectors.toList());
//			if(productInfo.size()==1) {
//				return productInfo.get(0);
//			}else {
//				return null;
//			}
//			for(Product pp : listOfProduct) {
//				if(pp.getPid()==pid) {
//					return pp;
//				}
//			}
//			return null;
		}
		
		public String deleteProductInfo(int pid) {
			int flag = 0;
			for(Product pp : listOfProduct) {
				if(pp.getPid()==pid) {
					listOfProduct.remove(pp);
					flag++;
					break;
				}
			}
			
			if(flag==0) {
				return "Product not present";
			}else {
				return "Product deleted successfully";
			}
		}
		
		
		public String updateProductInfo(Product p) {
			int flag = 0;
			for(Product pp : listOfProduct) {
				if(pp.getPid()==p.getPid()) {
				  pp.setPrice(p.getPrice());			// replacing old price by new price.
				//	pp.setPrice(pp.getPrice()+p.getPrice());     // increment the price 
				//	pp.setPrice(pp.getPrice()-p.getPrice());			// decrement the price 
					flag++;
					break;
				}
			}
			
			if(flag==0) {
				return "Product not present";
			}else {
				return "Product updated successfully";
			}
		}
		
}
