package com;
import java.sql.*;
public class DemoTest {

	public static void main(String[] args) {
		try {
			//Class.forName("com.mysql.jdbc.Driver");  // 5 version 
			Class.forName("com.mysql.cj.jdbc.Driver");  // 8 version 
			System.out.println("Driver Loaded...");
		Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl?useSSL=true", "root", "root@123");
		System.out.println("Establish the connection successfully");
		PreparedStatement pstmt = con.prepareStatement("insert into emp values(?,?,?)");
		pstmt.setInt(1, 7);
		pstmt.setString(2, "Mahesh");
		pstmt.setFloat(3, 18000);
		int res = pstmt.executeUpdate();
		if(res>0) {
			System.out.println("Record inserted successfully");
		}
		//Statement stmt = con.createStatement();
		// insert query 
//		int res = stmt.executeUpdate("insert into emp values(6,'Dinesh',34000)");
//		if(res>0) {
//			System.out.println("Record inserted successfully");
//		}
//		ResultSet rs = stmt.executeQuery("show tables");
//		while(rs.next()) {
//			String name = rs.getString(1);
//			System.out.println(name);
//		}
		// Delete query 
//		DatabaseMetaData dbd = con.getMetaData();
//		//System.out.println(); 
//		ResultSet rs =  dbd.get
//		while(rs.next()) {
//			System.out.println(rs.getString(1));
//		}
		// Delete Query 
//		int res = stmt.executeUpdate("delete from emp where id = 1");
//		if(res>0) {
//			System.out.println("Record deleted successfully");
//		}else {
//			System.out.println("Record didn't delete");
//		}
		// Update Query 
//		int res = stmt.executeUpdate("update emp set salary = 45000 where id=1");
//		if(res>0) {
//			System.out.println("Record updated successfully");
//		}else {
//			System.out.println("Record didn't update");
//		}
		// Retrieve the recods 
//			ResultSet rs	= stmt.executeQuery("select * from emp");
//			while(rs.next()) {
//								//int id = rs.getInt(1);
//									int id = rs.getInt("id");
//								String name = rs.getString(2);
//								float salary = rs.getFloat(3);
//								System.out.println(" id is "+id+" Name is "+name+" Salary is "+salary);
//			}
//			rs.close();
//			stmt.close();
//			con.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
