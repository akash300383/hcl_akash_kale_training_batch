package com.greatlearning.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.Product;
import com.greatlearning.resource.DbResource;

public class ProductDao {
			public int storeProductInfo(Product prod) {
				try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root@123");
				PreparedStatement pstmt = con.prepareStatement("insert into product values(?,?,?)");
				pstmt.setInt(1, prod.getPid());
				pstmt.setString(2, prod.getPname());
				pstmt.setFloat(3, prod.getPrice());
				int res  = pstmt.executeUpdate();
				return res;
				} catch (Exception e) {
					System.out.println("In store method "+e);
					return 0;
				}
			}
			
			public int deleteProduct(int pid) {
				try {
				//Class.forName("com.mysql.cj.jdbc.Driver");
				//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root@123");
				Connection con = DbResource.getDbConnection();
				PreparedStatement pstmt = con.prepareStatement("delete from product where pid = ?");
				pstmt.setInt(1, pid);
				int res  = pstmt.executeUpdate();
				return res;
				} catch (Exception e) {
					System.out.println("In delete method "+e);
					return 0;
				}
			}
			
			public int udateProduct(Product prod) {
				try {
				//Class.forName("com.mysql.cj.jdbc.Driver");
				//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root@123");
					Connection con = DbResource.getDbConnection();
				PreparedStatement pstmt = con.prepareStatement("update product set price = ? where pid =?");
				pstmt.setFloat(1, prod.getPrice());
				pstmt.setInt(2, prod.getPid());
				int res  = pstmt.executeUpdate();
				return res;
				} catch (Exception e) {
					System.out.println("In delete method "+e);
					return 0;
				}
			}
			
			public float findProductPrice(int pid) {
				try {
				//Class.forName("com.mysql.cj.jdbc.Driver");
				//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root@123");
					Connection con = DbResource.getDbConnection();
					PreparedStatement pstmt = con.prepareStatement("select price from product where pid=?");
				pstmt.setInt(1, pid);
				ResultSet rs = pstmt.executeQuery();
				if(rs.next()) {
					float price  = rs.getFloat(1);
					return price;
				}
				} catch (Exception e) {
					System.out.println("In delete method "+e);
				}
				return 0;
			}
			
			public List<Product> findAllProduct() {
				List<Product> listOfProduct = new ArrayList<>();
				try {
					//Class.forName("com.mysql.cj.jdbc.Driver");
					//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root@123");
					Connection con = DbResource.getDbConnection();
					PreparedStatement pstmt = con.prepareStatement("select * from product");
					ResultSet rs = pstmt.executeQuery();
					while(rs.next()) {
							Product p = new Product();
							p.setPid(rs.getInt(1));
							p.setPname(rs.getString(2));
							p.setPrice(rs.getFloat(3));
							listOfProduct.add(p);
					}
					} catch (Exception e) {
						System.out.println("In delete method "+e);
					}
				return listOfProduct;
			}
			
			public Product findProduct(int pid) {
				try {
					//Class.forName("com.mysql.cj.jdbc.Driver");
					//Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl","root","root@123");
					Connection con = DbResource.getDbConnection();
					PreparedStatement pstmt = con.prepareStatement("select * from product where pid = ?");
					pstmt.setInt(1, pid);
					ResultSet rs = pstmt.executeQuery();
					if(rs.next()) {
							Product p = new Product();
							p.setPid(rs.getInt(1));
							p.setPname(rs.getString(2));
							p.setPrice(rs.getFloat(3));
							return p;
					}
					} catch (Exception e) {
						System.out.println("In findProduct method "+e);
					}
					return null;
			}
}
