package com;
class Employee {
	private Employee() {
		
	}
	public void display() {
		System.out.println("This is display method of Employee class");
	}
	public static Employee getInstance(int code) {
		if(code==1) {
			Employee emp = new Employee();
			return emp;	
		}else {
			return null;
		}
		
	}
}
public class StaticFactoryMethodDesignPattern {
	public static void main(String[] args) {
		//Employee emp = new Employee();
		//emp.display();
		Employee employee = Employee.getInstance(2);
		employee.display();
	}
}
