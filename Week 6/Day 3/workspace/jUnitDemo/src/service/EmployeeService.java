package service;

import java.util.ArrayList;
import java.util.List;

import bean.Employee;

public class EmployeeService {

	public String checkLogin(String user,String pass) {
		if(user.equals("Raj") && pass.equals("123")) {
			return "success";
		}else {
			return "failure";
		}
	}
	public String sayHello(String name) {
		return "Welcome "+name;
	}
	public float updateSalary(int id, String name, float salary, String desg) {
		if(desg.equals("Manager")) {
			return salary+5000;
		}else if(desg.equals("Developer")) {
			return salary+3000;
		}else {
			return salary+1000;
		}
	}
	public String passEmployeeInfo(Employee emp) {
		return emp.getName();
	}
	public Employee getEmp() {
		Employee emp = new Employee(100, "Ramesh", 12000);
		return emp;
	}
	public List<Employee> getAllEmployee() {
		List<Employee> listOfEmp = new ArrayList<>();
		listOfEmp.add(new Employee(1, "Ramesh", 12000));
		listOfEmp.add(new Employee(2, "Ajay", 15000));
		listOfEmp.add(new Employee(3, "Vijay", 18000));
		listOfEmp.add(new Employee(4, "Lokesh", 22000));
		return listOfEmp;
	}
}
