package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import bean.Employee;
import service.EmployeeService;

public class EmployeeServiceTest {

	//@Test
	public void testCheckLogin() {
		//fail("Not yet implemented");
		EmployeeService es = new EmployeeService();
		String successResult = es.checkLogin("Raj","123");
		String failureResult = es.checkLogin("Ram", "1100");
		assertEquals("success", successResult);
		assertEquals("failure", failureResult);
	}

	//@Test
	public void testSayHello() {
	//	fail("Not yet implemented");
		EmployeeService es = new EmployeeService();
		String msg = es.sayHello("Raj");
		assertTrue(msg.endsWith("Raj"));
	}

	//@Test
	public void testUpdateSalary() {
		//fail("Not yet implemented");
		EmployeeService es = new EmployeeService();
		float managerSalary = es.updateSalary(100, "Ramesh", 45000, "Manager");
		assertEquals(50000, managerSalary,0.2);
	}

	//@Test
	public void testPassEmployeeInfo() {
		//fail("Not yet implemented");
	}

	//@Test
	public void testGetEmp() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetAllEmployee() {
		//fail("Not yet implemented");
		EmployeeService es = new EmployeeService();
		List<Employee> listOfEmp = es.getAllEmployee();
		assertEquals(4, listOfEmp.size());
		Employee emp = listOfEmp.get(0);
		assertEquals(1, emp.getId());
		assertEquals("Ramesh", emp.getName());
		assertEquals(12000, emp.getSalary(),0.2);
	}

}
