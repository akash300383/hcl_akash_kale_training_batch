package com;
class Volt {
		private int volts;
		public Volt(int volts) {
			super();
			this.volts = volts;
		}
		public int getVolts() {
			return volts;
		}
		public void setVolts(int volts) {
			this.volts = volts;
		}
}
class Socket {
		public Volt getVolts() {
			Volt vv = new Volt(120);
			return vv;
		}
}
interface SocketAdapter {
	public Volt get3Volt();
	public Volt get12Volt();
	public Volt get120Volt();
}
class SocketAdapterImp implements SocketAdapter {
	Socket ss = new Socket();
	public Volt get3Volt() {
		Volt vv = ss.getVolts();
		return new Volt(vv.getVolts()/40);
	}
public Volt get12Volt() {
		Volt vv = ss.getVolts();
		return new Volt(vv.getVolts()/10);
	}
	public Volt get120Volt() {
		return ss.getVolts();
	}
}
public class AdapterDesignPattern {

	public static void main(String[] args) {
		SocketAdapter adapter = new SocketAdapterImp();
		Volt v1 = adapter.get120Volt();
		System.out.println(v1.getVolts());
		Volt v2 = adapter.get12Volt();
		System.out.println(v2.getVolts());
		Volt v3 = adapter.get3Volt();
		System.out.println(v3.getVolts());
	}

}
