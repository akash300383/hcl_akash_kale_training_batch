package com.greatlearning.assignment.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.greatlearning.assignment.bean.Employee;

public class EmployeeService {
	List<Employee> listOfEmp = new ArrayList<>();
	public EmployeeService() {
		listOfEmp.add(new Employee(1, "Raj", 12000, LocalDate.of(2001, 1, 10)));
		listOfEmp.add(new Employee(2, "Ramesh", 15000, LocalDate.of(2003, 10, 30)));
		listOfEmp.add(new Employee(3, "Rajesh", 13000, LocalDate.of(2001, 6, 5)));
		listOfEmp.add(new Employee(4, "Lokesh", 18000, LocalDate.of(2000, 4, 22)));
	}
	public String addEmployee(Employee emp) {
		boolean flag=false;
		Iterator<Employee> li = listOfEmp.iterator();
		while(li.hasNext()) {
			Employee employee = li.next();
			if(employee.getId()==emp.getId()) {
					flag=true;
					break;
			}
		}
		if(flag) {
			return "Employee id must be unique";
		}else {
			listOfEmp.add(emp);
			return "Record stored successfully";
		}
	}
	
	public List<Employee> getAllEmployeeDetails() {
		return listOfEmp;
	}
	
	public String deleteEmployeeInfo(int id) {
			boolean flag=false;
			Iterator<Employee> li = listOfEmp.iterator();
			while(li.hasNext()) {
				Employee emp= li.next();
					if(emp.getId()==id) {
						listOfEmp.remove(emp);
						flag = true;
						break;
					}
			}
			if(flag) {
				return "Record deleted successfully";
			}else {
				return "Recod not present";
			}
	}
	
}
