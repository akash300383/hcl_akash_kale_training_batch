package com;

public class VolatileExample {
   static volatile boolean val = true;					// only one copy of val variable 
	public static void main(String[] args) {
		VolatileExample.FirstThread ff = new VolatileExample().new FirstThread();			// value part of different memory 
		VolatileExample.SecondThread ss = new VolatileExample().new SecondThread();	// value part of different memory 
		ff.start();
		ss.start();
	}
	class FirstThread extends Thread {
		public void run() {
				while(true) {
						if(val) {
						System.out.println("First thread is running ");
						val = false;					// value is false 
					}
				}
		}
	}
	class SecondThread extends Thread {
		public void run() {
			while(true) {
				if(!val) {
					System.out.println("Second thread is running");
					try {
						Thread.sleep(1000);
					}catch (Exception e) {
						// TODO: handle exception
					}
					val = true;				// thread made value is true. but value didn't update for static value variable. val true in cache 
				}									// memory not in main memory. 
			}
		}
	}
	
}
