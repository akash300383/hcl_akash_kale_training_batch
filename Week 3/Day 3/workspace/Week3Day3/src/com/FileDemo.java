package com;

import java.io.File;

public class FileDemo {

	public static void main(String[] args) {
		File ff = new File("C:\\test.txt");
		System.out.println(ff.getAbsolutePath());
		System.out.println(ff.getName());
		System.out.println(" is file "+ff.isFile());
		System.out.println(" is directory "+ff.isDirectory());
		System.out.println(" read "+ff.canRead());
		System.out.println("write "+ff.canWrite());
		System.out.println("delete "+ff.delete());
		
	}

}
