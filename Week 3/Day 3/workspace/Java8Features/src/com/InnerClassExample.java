package com;
class Outer {
			int a;
			void dis1() {
				System.out.println("non static method of outer class");
			}
			class NonStaticInner {				// number of outer class object equal to number inner class object 
						int b;
						void dis2() {
							System.out.println("Non static inner class dis2() method");
						}
			}
			static class StaticInner {					// only one copy of static inner class 
					int c;
					void dis3() {
						System.out.println("Static inner class dis3() method");
					}
			}
}
public class InnerClassExample {
	public static void main(String[] args) {
		Outer out = new Outer();
		out.dis1();
		Outer.NonStaticInner  in1 = new Outer().new NonStaticInner();		// non static inner class
		in1.dis2();
		Outer.StaticInner in2 = new Outer.StaticInner();							// static inner class 
		in2.dis3();
	}
}
