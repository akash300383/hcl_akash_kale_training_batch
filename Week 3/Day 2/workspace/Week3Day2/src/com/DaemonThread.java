package com;
class Abc implements Runnable {
	public void run() {
			Thread t = Thread.currentThread();
			String name = t.getName();
			for(int i=0;i<10;i++) {
				try {
				System.out.println(name +" "+i);
				//Thread.sleep(500);
				}catch (Exception e) {}
			}
	}
}
public class DaemonThread {
	public static void main(String[] args) {
	Abc obj1 = new Abc();
	Thread t1 = new Thread(obj1); 		t1.setName("1st Thread"); 
	Thread t2 = new Thread(obj1);		t2.setName("2nd Thread");
	t1.setDaemon(true);
	t2.setDaemon(true);
	t1.start(); 
	t2.start();
	System.out.println(" Main Thread");			// this code execute first but main thread doesn't destory before all child thread 
	}
}
