package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class StreamApiExample {

	public static void main(String[] args) {
		List<String> names = new ArrayList<>();
		names.add("Raj"); names.add("Ramesh"); names.add("Ajay");
		names.add("Vijay"); names.add("Dinesh"); names.add("Lokesh");
//		System.out.println("Before Java 8 ");
//		Iterator<String> li = names.iterator();
//		while(li.hasNext()) {
//			String name = li.next();
//			if(name.startsWith("R")) {
//				System.out.println("name is "+name);
//			}
//		}
//		System.out.println("After Java 8 - Multiple line");
//		Stream<String> ss = names.stream();						// converting to Stream 
//		Stream <String> ss1	= ss.filter(v->v.startsWith("R"));		// using one Intermediate operator 
//		ss1.forEach(v->System.out.println("name is "+v));				// using terminal operator 
//		System.out.println("After Java 8 - Using lambda expression in one line");
		
		// Display all names from list with condition name start with R character 
		//names.stream().filter(v->v.startsWith("R")).forEach(v->System.out.println("Name is "+v));
		
		// display all names in lower case using stream API.
		//names.stream().map(v->v.toLowerCase()).forEach(v->System.out.println("name "+v));
		
		// display all name length form list
		//names.stream().mapToInt(v->v.length()).forEach(leng->System.out.println("Each name number of character "+leng));
		
		// display names in sorting order. 
		//names.stream().sorted().forEach(v->System.out.println(v));
		
		long numberOfNames = names.stream().filter(v->v.contains("e")).count();
		System.out.println("Number of name contains e character "+numberOfNames);
		boolean res1 = names.stream().anyMatch(v->v.endsWith("j"));
		System.out.println(" Result is "+res1);
		boolean res2 = names.stream().allMatch(v->v.length()>4);
		System.out.println(" Result is "+res2);
	}

}
