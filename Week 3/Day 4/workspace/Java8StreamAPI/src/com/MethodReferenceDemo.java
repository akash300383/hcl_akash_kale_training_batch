package com;

import java.util.ArrayList;
import java.util.List;

interface Operation {
	int add(int x, int y);
}
class StaticMethodReference {
	public static int add(int x, int y) {
		int sum = x+y;
		return sum;
	}
}
class NonStaticMethodReference {
	public int add(int x, int y) {
		int sum = x+y;
		return sum;
	}
}
public class MethodReferenceDemo {
	public static void main(String[] args) {
		// using lambda expression provide the body for functional interface. 
		Operation op1 = (a,b)->a+b;			
		System.out.println(op1.add(100, 200));
		// Static method reference 
		Operation op2 = StaticMethodReference::add;			// static method access through class name
		System.out.println(op2.add(10, 20));
		NonStaticMethodReference obj = new NonStaticMethodReference();
		Operation op3 =  obj::add;								// non static method can access through object of that class. 
		System.out.println(op3.add(1, 2));

		List<Integer> list = new ArrayList<>();
		list.add(10); list.add(20); list.add(30); list.add(40);
		// retreive the records using lambda expression 
		System.out.println("Display all element using lambda expression");
		list.forEach(v->System.out.println(v));			// using lambda expression 
		System.out.println("Display all elements using method reference");
		list.forEach(System.out::println);					// using static method reference. 
	}

}




