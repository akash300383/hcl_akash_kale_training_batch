package com;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Set;

public class DateApiExample {

	public static void main(String[] args) {
		LocalDate ld1 = LocalDate.now();
		System.out.println(ld1);
		LocalTime lt1 = LocalTime.now();
		System.out.println(lt1);
		LocalDateTime ldt1 = LocalDateTime.now();
		System.out.println(ldt1);
		// Custom Date 
		LocalDate ld2 = LocalDate.of(2021, 12, 10);
		System.out.println(ld2);
//		Set<String> ss = 	ZoneId.getAvailableZoneIds();
//		Iterator<String> li = ss.iterator();
//		while(li.hasNext()) {
//			String zoneInfo = li.next();
//			System.out.println(zoneInfo);
//		}
		
		LocalDateTime ldt2 = LocalDateTime.now(ZoneId.of("Asia/Kolkata"));
		System.out.println(ldt2);
		LocalDateTime ldt3 = LocalDateTime.now(ZoneId.of("America/Santarem"));
		System.out.println(ldt3);
		LocalDateTime ldt4 = LocalDateTime.now();
		System.out.println("Default format of Date and time "+ldt4);
		System.out.println("Custom Date format "+ldt4.format(DateTimeFormatter.ofPattern("dd-MM-yy")));
		System.out.println("Custom Date format "+ldt4.format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
		System.out.println("Custom Time "+ldt4.format(DateTimeFormatter.ofPattern("hh:mm:ss")));
		System.out.println("Custom Time "+ldt4.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
		
		System.out.println("Custom Time "+ldt4.format(DateTimeFormatter.ofPattern("day")));
		
	}

}
