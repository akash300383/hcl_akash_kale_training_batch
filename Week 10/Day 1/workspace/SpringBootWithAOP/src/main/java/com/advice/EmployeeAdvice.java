package com.advice;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect							// This class contains all cross cutting logic 
public class EmployeeAdvice {

	@Pointcut("execution(public String com.service.EmployeeService.*(..) )")
	public void myJoinPoint() {
		
	}
	@Pointcut("execution(public String com.service.EmployeeService.*(..) )")
	public void myJoinPoint1() {
		
	}
	@Before("myJoinPoint()")
	public void beforeAdvice() {
		System.out.println("This method executed before business method");
		// check database connectivity 
		// check logic details 
		// check security 
	}
	
	@After("myJoinPoint()")
	public void afterAdvice() {
		System.out.println("This method executed after business method");
		// close the resources 
	}
	@Around("myJoinPoint()")
	public void aroundAdvice(ProceedingJoinPoint jp) {
		System.out.println("Before in aroundAdvice");
		try {
				jp.proceed();					// call businsess method 
		} catch (Throwable e) {
			// TODO: handle exception
		}
		System.out.println("After in aroundAdvice");
	}
	
	@AfterThrowing("myJoinPoint()")
	public void throwException() {
		System.out.println("it will execute only if any exception in business method generate");
	}
}
