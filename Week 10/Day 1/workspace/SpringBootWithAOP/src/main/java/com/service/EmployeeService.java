package com.service;

import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

	public String simpleEmployeeMethod() {
		System.out.println("This is actual business method");
		//try {
		//int a=10/0;
		//}catch(Exception e) {}
		return "This is simple employee business method";
	}
}
