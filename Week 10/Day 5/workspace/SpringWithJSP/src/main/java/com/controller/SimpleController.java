package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SimpleController {

	@GetMapping(value = "/")
	public String openIndexPage() {
		System.out.println("I came here");
		return "index";
	}
	@GetMapping(value = "loginPage")
	public String loginPage() {
		
		return "login";
	}
}
