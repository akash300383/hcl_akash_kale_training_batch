package xyz;

public class TryWithMultipleCatchBlock {

	public static void main(String[] args) {
		try {
			int abc[]= {0,1,2,3,4,5};
			int res = 10/abc[2];
			System.out.println("Result is "+res);
			String str = "10a";
			System.out.println(str+10);     // output as 1010
			int n = Integer.parseInt(str);		// converting string to integer 
			System.out.println(n+10);			//output is 20
		} catch (ArithmeticException e) {
				System.out.println("Divided by Zero "+e);
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array Index "+e);
		}catch (Exception e) {
				System.out.println("Generic Exception "+e);
		}
		System.out.println("Finish");
	}

}
