package xyz;

public class TryWithFinally {

	public static void main(String[] args) {
		try {
			int a=10/0;
			System.out.println("No Exception");
		} finally {
			System.out.println("finally block");
		}
		System.out.println("Normal Statement");
	}

}
