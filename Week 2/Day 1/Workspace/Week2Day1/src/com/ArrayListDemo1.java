package com;

import java.util.ArrayList;

public class ArrayListDemo1 {
	public static void main(String[] args) {
		ArrayList al = new ArrayList();
		System.out.println("Size "+al.size());
		System.out.println("Empty "+al.isEmpty());
		al.add(100);
		al.add(200);
		al.add(300);
		al.add(400);
		System.out.println("Size "+al.size());
		System.out.println("Empty "+al.isEmpty());
		System.out.println(al);
		System.out.println("Get number using index position "+al.get(1));
		al.add(1, 1000);		//1 position adding 1000
		System.out.println("Get number using index position "+al.get(1));
		System.out.println(al);
		al.set(2, 2000);    // replace the 2nd index position value by 2000
		System.out.println(al);
		al.remove(2);
		System.out.println(al);
		System.out.println("Search "+al.contains(100));
		System.out.println("Search "+al.contains(10));
	}

}
