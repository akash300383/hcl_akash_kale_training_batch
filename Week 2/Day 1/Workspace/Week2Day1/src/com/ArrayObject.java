package com;
import java.util.Scanner;
public class ArrayObject {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("how many records do you want to store?");
		int n = sc.nextInt();
		Employee employees[]=new Employee[n];
		System.out.println("enter the details one by one");
		for(int i=0;i<n;i++) {
//			employees[i]=new Employee();		// empty constructor 
//			System.out.println("Enter the id");
//			int id = sc.nextInt();
//			employees[i].setId(id);
//			System.out.println("Enter the name");
//			String name = sc.next();
//			employees[i].setName(name);
//			System.out.println("Enter the salary");
//			float salary = sc.nextFloat();
//			employees[i].setSalary(salary);
			
			System.out.println("Enter the id");
			int id = sc.nextInt();
			System.out.println("Enter the name");
			String name = sc.next();
			System.out.println("Enter the salary");
			float salary = sc.nextFloat();
			employees[i]=new Employee(id, name, salary);		// parameterized constructor called.			
		}
		System.out.println("All employees details are ");
		for(int i=0;i<n;i++) {
			//System.out.println(employees[i].getId()+" "+employees[i].getName()+" "+employees[i].getSalary());
			System.out.println(employees[i]);    // it will call toString() methods. 
		}
	}
}
