package com;

import java.util.Stack;

public class StackDemo {

	public static void main(String[] args) {
		Stack ss = new Stack();
		ss.push(100);
		ss.push(200);
		ss.push(300);
		ss.push(400);
		System.out.println(ss);
		System.out.println("To remove top most element is "+ss.pop());		// it remove top most element 
		System.out.println(ss);
		System.out.println("To check top most element is "+ss.peek());		// display only top most element but doesn't remove 
		System.out.println(ss);
		System.out.println("Search operation "+ss.search(100));  // if present display the position and count start from top 
		System.out.println("Search operation "+ss.search(1000));	// if not present display -1
		System.out.println("Size "+ss.size());
		ss.remove(2);
		System.out.println(ss);
	}

}
