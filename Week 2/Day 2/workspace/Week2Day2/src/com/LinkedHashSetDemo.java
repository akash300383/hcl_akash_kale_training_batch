package com;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//HashSet obj = new HashSet();
		LinkedHashSet obj = new LinkedHashSet();
		obj.add(10);
		obj.add(40);
		obj.add(20);
		obj.add(30);
		obj.add(50);
		obj.add(60);
		System.out.println(obj);
		Iterator ii = obj.iterator();
		while(ii.hasNext()) {
			Object o = ii.next();
			System.out.println(o);
		}
	}

}
