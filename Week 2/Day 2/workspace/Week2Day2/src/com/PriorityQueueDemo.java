package com;

import java.util.PriorityQueue;

public class PriorityQueueDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		PriorityQueue pq = new PriorityQueue();
		pq.add(3);
		pq.add(2);
		pq.add(1);
		pq.add(4);
		pq.add(6);
		//pq.add(2);
		System.out.println(pq);		//lower priority number come first 
		System.out.println(pq.poll());	// remove lower priority element ie 1
		System.out.println(pq);
		System.out.println(pq.poll());  // remove lower priority element ie 2
		System.out.println(pq);
		System.out.println(pq.peek());		// display first element doesn't remove 
		System.out.println(pq);
	}

}
