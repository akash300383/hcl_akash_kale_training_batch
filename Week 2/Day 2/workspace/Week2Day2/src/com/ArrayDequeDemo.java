package com;

import java.util.ArrayDeque;

public class ArrayDequeDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque ad = new ArrayDeque();
		ad.add(10);			// adding the elemet at the last 
		ad.add(20);
		ad.add(30);
		System.out.println(ad);
		ad.addFirst(100);		// at begining 
		ad.addLast(200);		// at the last 
		System.out.println(ad);
		ad.removeFirst();		// remove from begining  
		ad.removeLast();		// remove from last 
		System.out.println(ad);
		ad.remove(20);			// between remove base upon teh value. 
		System.out.println(ad);
		//al.remove(new Integer(10));
		// al.remove(10);		it consider index 10.
	}

}
