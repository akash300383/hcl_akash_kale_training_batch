package com;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CollectionWithGenerics {

	public static void main(String[] args) {
		// Without Generics 
//		List ll = new ArrayList(); 
//		ll.add(10);			// auto-boxing : int primitive to Object 
//		ll.add("Raj");
//		ll.add(10.10);
//		ll.add(20);
//		
//		Object obj = ll.get(2);
//		Integer i = (Integer)obj;	// downlevel type casting for integer object 
//		int n = i.intValue();		// using interger object we have to convert object to primitive 
//		System.out.println(n);
		// Collection Framework with generics 
		List<Integer> ll = new ArrayList<>();
		ll.add(10);			// auto-boxing - converting primitive to Integer object. 
		ll.add(20);
		//ll.add("Raj");
		//ll.add(10.20);
		ll.add(30);
		ll.add(40);
		
		int n=ll.get(2);	// auto-unboxing : converting Integer to int primitive 
		System.out.println(n);
		Map<Integer, String> mm = new HashMap<>();
		mm.put(100, "Raj");
		mm.put(101, "Ramesh");
		System.out.println(mm);
	}

}
