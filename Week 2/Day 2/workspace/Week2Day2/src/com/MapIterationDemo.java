package com;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class MapIterationDemo {

	public static void main(String[] args) {
		HashMap hm  = new HashMap();
		hm.put(2, "Raj");
		hm.put(1, "Ajay");
		hm.put(3,"Vijay");
		hm.put(4, "Kishor");
		Set ss =hm.entrySet();		// converting map to set 
		Iterator ii = ss.iterator();
		while(ii.hasNext()) {
			//Object obj = ii.next();
			//System.out.println(obj);
			Map.Entry me = (Map.Entry)ii.next();	// Map is outer interface and Entry is inner interface. 
			System.out.println("Key "+me.getKey());
			System.out.println("Value "+me.getValue());
			System.out.println("using key get the value "+hm.get(me.getKey()));
		}
	}

}
