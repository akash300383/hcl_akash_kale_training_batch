package com;

import java.util.HashMap;

public class HashMapDemo {
	public static void main(String[] args) {
		HashMap hm = new HashMap();
		hm.put(2,"Raj");
		hm.put(1,"Ramesh");
		hm.put(3, "Ajay");
		hm.put(4, "Balaji");
		System.out.println(hm);
		hm.put(1, "Mahesh");	// value get replaced. 
		hm.put(6, "Ajay");
		System.out.println(hm);
		System.out.println(hm.containsKey(1));
		System.out.println(hm.containsValue("Ramesh"));
		System.out.println(" VAlue is "+hm.get(2));
		System.out.println("Value is "+hm.get(10));
		System.out.println("Remove "+hm.remove(3));
		System.out.println("Remove "+hm.remove(30));
		System.out.println(hm);
	}
}
