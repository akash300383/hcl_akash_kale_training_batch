package com;

import java.util.Arrays;

public class ArraysDemo {
	public static void main(String[] args) {
		int abc[]= {2,5,1,3,7,5,4,9};
		System.out.println("Before sort");
		for(int a:abc) {
			System.out.print(a+" ");
		}
		Arrays.sort(abc);
		System.out.println();
		System.out.println("After sort");
		for(int a:abc) {
			System.out.print(a+" ");
		}
		System.out.println("");
		System.out.println(" Search "+Arrays.binarySearch(abc, 1));			// it present return the index position 
		System.out.println("Search "+Arrays.binarySearch(abc, 12));		// it not present return -ve value. 
		if(Arrays.binarySearch(abc, 6)<0) {
					System.out.println("Not present");
		}else {
			System.out.println("It is present");
		}
	}

}
