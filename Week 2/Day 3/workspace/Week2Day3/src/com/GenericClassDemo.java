package com;
class Abc {
			void add(int x, int y) {
					System.out.println("int parameter");
					System.out.println(x+" "+y);
			}
			void add(String x, String y) {
				System.out.println("string parameter");
				System.out.println(x+" "+y);
			}
			void add(float x, float y) {
				System.out.println("float parameter");
				System.out.println(x+" "+y);
			}
}
class Xyz<T>{				// Generic class 
			void add(T x, T y) {			//Generic Method
				System.out.println("Generic parameter");
				//System.out.println(x+y);
			}
}
public class GenericClassDemo {
	public static void main(String[] args) {
		Abc obj = new Abc();
		obj.add(10, 20);
		obj.add("1", "2");
		obj.add(10.10f, 20.30f);
		Xyz obj1 = new Xyz();
		obj1.add(1, 2);
		obj1.add(10.10, 20.20);
		obj1.add("1", "2");
		obj1.add('a', 'b');
		obj1.add(1, "2");
	}

}
