package com;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProductTest {
	public static void main(String[] args) {
		//List listOfProducts = new ArrayList();
		List<Product> listOfProducts = new ArrayList<>();
		Product p1 = new Product();
		p1.setPid(100);
		p1.setPname("Tv");
		p1.setPrice(85000);
		Product p2 = new Product(101, "Laptop", 120000);
		System.out.println("Number of product "+listOfProducts.size());
		listOfProducts.add(p1);
		listOfProducts.add(p2);					// first created and then added. 
		listOfProducts.add(new Product(102, "Computer", 45000));		// creating and adding 
		//listOfProducts.add(10);
		//listOfProducts.add("Raj");
		System.out.println("Number of products "+listOfProducts.size());		
			//Object obj = listOfProducts.get(3);
			//Product pp = (Product)obj;
			Product pp = listOfProducts.get(0);
			System.out.println(pp);			// internally call which method toString()
			listOfProducts.remove(1);
			System.out.println("Size of products are "+listOfProducts.size());
			System.out.println(listOfProducts);
			Iterator<Product> li = listOfProducts.iterator();
			while(li.hasNext()) {
				Product p  = li.next();
				//System.out.println(p);                 // toString() 
				System.out.println("Name is "+p.getPname()+" Price "+p.getPrice());
			}
			
	}

}
