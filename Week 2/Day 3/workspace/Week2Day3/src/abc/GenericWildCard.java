package abc;
import java.util.HashSet;
import java.util.Set;
class Abc {
	void display1(Set<Integer> ss) {
			System.out.println("Set of" +ss.size());			// only integer 
	}
	void display2(Set<String> ss) {
			System.out.println("set of string "+ss.size());		// only String 
	}
	void display3(Set<Double> ss) {
			System.out.println("Set of double  "+ss.size());		// onlye double 
	}
	void display4(Set<?> ss) {				// any type Set reference can receive. 
		System.out.println("set of raw type "+ss.size()); // can be any type int or string or double 
	}
	void display5(Set<? extends Number> ss) {		// any class it must be sub class of Number
		System.out.println("any class which extends Number class");	
	}
	void display6(Set<? super String> ss) {			// any class including string as well as super class String class 
		System.out.println(" any class that is super class of String class ");
	}
	void display7(Set<?> ss) {							// we can pass anything 
																		// A, B and C 
	}
	void display8(Set<? extends B> ss) {		// anyclass which extends B class including B class 
																			// same class ie B and sub class of B class
																			// B and C 
	}
	void display9(Set<? super B> ss) {			// any class which super class of B class 
																		// same class ie B and super class of B ie A
	}
}
class A {}
class B extends A{}
class C extends B{}
public class GenericWildCard {
	public static void main(String[] args) {
		A obj1 = new A();
		B obj2 = new B();
		C obj3 = new C();
		Set<A> aSet = new HashSet<>(); aSet.add(obj1);		
		Set<B> bSet = new HashSet<>(); bSet.add(obj2);
		Set<C> cSet = new HashSet<>(); cSet.add(obj3);
		Abc obj = new Abc();
		obj.display7(aSet);		obj.display7(bSet);   obj.display7(cSet);		// it is ? raw type 
		obj.display8(bSet);    obj.display8(cSet);   			// ? extends B means we can pass B as well as C
		obj.display9(aSet);    obj.display9(bSet);             // ? super B means we can pass B class as well as A class 
		
//		Set<Integer> s1 = new HashSet<>(); s1.add(10); s1.add(20); s1.add(30);
//		Set<String> s2 = new HashSet<>(); s2.add("Raj"); s2.add("Ravi"); s2.add("Mahesh");
//		Set<Double> s3 = new HashSet<>();s3.add(10.10); s3.add(20.20); s3.add(30.30);
//		Abc obj = new Abc();
//		obj.display1(s1);		obj.display2(s2);   obj.display3(s3);   
//		obj.display4(s1); 	obj.display4(s2); obj.display3(s3);				// we can pass raw type. 
//		obj.display5(s1);    obj.display5(s3);
//		obj.display6(s2);		
	}

}
