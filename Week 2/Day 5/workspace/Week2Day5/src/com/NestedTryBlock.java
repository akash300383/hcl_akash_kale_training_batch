package com;

public class NestedTryBlock {
	public static void main(String[] args) {
		
				try {
							int a=10/0;
								try {
									int a=10/0;			// inner catch block can handle 
									//int abc[]= {10,20,30,40};
									//int res = 100/abc[5];				// outer catch block can handle
									//String str = "a10";
									//int abc = Integer.parseInt(str);
								} catch (ArithmeticException e) {
									System.out.println("Inner catch block "+e);
								}
					
					
				}catch (ArithmeticException e) {
				
				}
				
				
				try {
					
				}catch (Exception e) {
					// TODO: handle exception
				}
				System.out.println("finish");
	}
}
