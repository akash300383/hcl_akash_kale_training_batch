package com;
class Bike {
		void speed() {
			System.out.println("60km/hr");
		}
}

class Pulsar extends Bike{
	@Override	
	void speed() {
		System.out.println("90km/hr");
	}
	void color() {
		System.out.println("Black");
	}
}
public class OverrideAnnotationDemo {
	public static void main(String[] args) {
		Pulsar pu = new Pulsar();
		pu.speed();
//		Bike bb = new Pulsar();
//		bb.speed();
	}

}
