import { Component } from '@angular/core';

@Component({
  selector: 'app-root',     // user-defined tag name <app-root></app-root>
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 name:string = "Raj Deep";
 id:number =100;
 salary:number = 12000.50;
 result:boolean = true;
}
