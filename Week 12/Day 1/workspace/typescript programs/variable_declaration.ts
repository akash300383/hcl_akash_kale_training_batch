var a=10;                   // int a=10;
a=20;                       // re-initialization a=20
var a=30;                   // re-declaration   int a=30; in java not possible. 

let b=10;
b=20;
//let b=30;                 // we can't do.
for(var i=0;i<1000;i++){

}
console.log(i);         // we can access i value outside loop 
for(let j=0;j<1000;j++){

}
//console.log(j);            // we can't access j value outside loop 

var c=300;
c=400;
const d=500;
//d=600;                    // we can't change the value. 
