import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-template-reference',
  templateUrl: './template-reference.component.html',
  styleUrls: ['./template-reference.component.css']
})
export class TemplateReferenceComponent implements OnInit {
  msg:string =""
  constructor() { }
  ngOnInit(): void {
  }
  checkUser(uRef:any,pRef:any): void {
    let user = uRef.value;
    let pass = pRef.value;
    if(user=="Raj" && pass=="123"){
      this.msg = "Successfully Login";
    }else {
      this.msg = "Failure try once again";
    }
  }
}
