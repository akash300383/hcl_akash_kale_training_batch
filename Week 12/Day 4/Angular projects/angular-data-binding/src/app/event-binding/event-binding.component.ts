import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-event-binding',
  templateUrl: './event-binding.component.html',
  styleUrls: ['./event-binding.component.css']
})
export class EventBindingComponent implements OnInit {
  msg:string="";
  constructor() { }

  ngOnInit(): void {    // life cycle method which call automatically.
  }

  fun():void {
    console.log("Event Fired..");
    this.msg = "Welcome to Angular Training";
    console.log(this.msg);
  }
}
