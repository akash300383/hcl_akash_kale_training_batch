import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-string-interpolation',
  templateUrl: './string-interpolation.component.html',
  styleUrls: ['./string-interpolation.component.css']
})
export class StringInterpolationComponent implements OnInit {
  fname:string ="Raj";
  a:number =10;
  b:number =20;
  constructor() { }

  ngOnInit(): void {
  }

  sayHello(name:string):string {
    return "Welcome user "+name;
  }
}
