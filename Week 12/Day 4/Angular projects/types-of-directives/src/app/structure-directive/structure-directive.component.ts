import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';

@Component({
  selector: 'app-structure-directive',
  templateUrl: './structure-directive.component.html',
  styleUrls: ['./structure-directive.component.css']
})
export class StructureDirectiveComponent implements OnInit {
  label:number =4;
  f1:boolean = true;
  f2:boolean = false;
  f3:boolean = false;
  flag:boolean = false;
  msg:string ="show";
  num:Array<Number>=[10,20,30,40,50,60];
  names:Array<string>=["Ravi","Ramesh","Lokesh","Ajay","Vijay"]
  employees:Array<Employee>=[];   // created employee array object. 
  constructor() { 
    let emp1 = new Employee(1,"Ravi",12000);
    let emp2 = new Employee(2,"Raju",14000);
    let emp3 = new Employee(3,"Ramesh",16000);
    let emp4 = new Employee(4,"Lokesh",20000);
    this.employees.push(emp1);
    this.employees.push(emp2);
    this.employees.push(emp3);
    this.employees.push(emp4);
  }

  ngOnInit(): void {
  }

  fun() {
    this.f3 = true;
  }

  toggle(){
    if(this.msg=="show"){
      this.flag = true;
      this.msg = "hide";
    }else {
      this.flag = false;
      this.msg = "show";
    }
  }
}
