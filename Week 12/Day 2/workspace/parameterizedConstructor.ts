// class Customer {
//     cid:number;                 // instance variable 
//     cname:string;
//     age:number;
//     constructor(cid:number,cname:string,age:number) {
//             this.cid=cid;
//             this.cname = cname;
//             this.age = age;
//     }
//     display():void {
//         console.log(" id is "+this.cid);
//         console.log("name is "+this.cname);
//         console.log("age is "+this.age)
//     }
// }
// let c1  = new Customer(100,"Ravi",21);
// c1.display();

// short cut constructor initialization 

class Customer {
    //cid:number;                 // instance variable 
    //cname:string;
    //age:number;
    // in type script we can't write more than one constructor 
    // we can write empty or parameter but only one
    constructor(private cid?:number,private cname?:string,public age?:number) {
            //this.cid=cid;
            //this.cname = cname;
            //this.age = age;
    }
    display():void {
        console.log(" id is "+this.cid);
        console.log("name is "+this.cname);
        console.log("age is "+this.age)
    }
}
let c1  = new Customer(100,"Ravi",21);
console.log(c1.age);
c1.display();
let c2 = new Customer(101,"Ramesh")
c2.display();