interface Abc {
    display():void;
}

class Sample implements Abc {
    display(): void {
        console.log("This is interface function")
    }
}

let s = new Sample();
s.display();