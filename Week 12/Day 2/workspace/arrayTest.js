// array initialization using literal style 
var num = [10, 20, 30, 40];
var names = ["Ravi", "Raj", "Ramesh"];
var info = [1, "Ravi", true, 12000.50];
// array initilization using generic style 
var num1 = [10, 20, 30, 40, 50];
var names1 = ["Ramesh", "RAvi", "Lokesh"];
var info1 = [1, "Ravi", true, 123.50];
// array methods 
var abc = [];
var xyz = [];
console.log(abc.length); // size is 0
console.log(xyz.length); // size is 0
abc.push(10); // it add the element at the last 
abc.push(20);
abc.push(30);
abc.unshift(100); // it add the element at the begining 
abc.unshift(200);
abc.unshift(300);
console.log(abc); // it display as a string. 
abc.pop(); // remove the elements from last 
abc.shift(); // remove the elements from begining 
console.log(abc);
// splice this method is use to add, remove and update array elements in between. 
//abc.splice(1,1);        // from index position 1 it will remove 1 elements.
//abc.splice(1,2);        // from index position 1 it will remove 2 elements. 
//abc.splice(1,0,11);         // add 11 element in 1 position 
abc.splice(1, 1, 11); // replace 1 position element by 11 
console.log(abc);
