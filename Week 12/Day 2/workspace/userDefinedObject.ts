// user-defined object creation in ES5 style 
// function Employee() {
//     this.id = 100;
//     this.fname ="Ravi";
//     this.age = 21;

//     this.displayInfo = function() {
//         console.log("id is "+this.id);
//         console.log("name is "+this.fname)
//         console.log("age is "+this.age);
//     }

// }
// var emp1 = new Employee();
// emp1.displayInfo();

//ES5 style 
function Employee() {
    this.id = 100;              // instance variable 
    this.fname = "Ravi";

    this.display = function () {
        console.log(" id is "+this.id);
        console.log("name is "+this.fname);
    }
}
var emp1 = new Employee();
emp1.display();
//ES6 style or typescript 
class Customer {
    id:number=100;              // instance variable 
    fname:string = "Ravi";
    constructor() {
        console.log("object created...")
    }
    display() : void {
        console.log("id is "+this.id);
        console.log("name is "+this.fname);
    }
}
let cust1 = new Customer();
cust1.display();







