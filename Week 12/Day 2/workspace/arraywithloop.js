var num = [10, 20, 30, 40, 50, 60];
console.log("classical for loop");
for (var i = 0; i < num.length; i++) {
    console.log(num[i]);
}
console.log("for in loop"); // it give index position 
for (var i in num) {
    console.log("Index position " + i + " Value is " + num[i]);
}
console.log("for of loop"); // it give value 
for (var _i = 0, num_1 = num; _i < num_1.length; _i++) {
    var n = num_1[_i];
    console.log("Value is " + n);
}
// using forEach function 
num.forEach(function (n) { return console.log(n); });
