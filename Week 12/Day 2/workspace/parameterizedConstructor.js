// class Customer {
//     cid:number;                 // instance variable 
//     cname:string;
//     age:number;
//     constructor(cid:number,cname:string,age:number) {
//             this.cid=cid;
//             this.cname = cname;
//             this.age = age;
//     }
//     display():void {
//         console.log(" id is "+this.cid);
//         console.log("name is "+this.cname);
//         console.log("age is "+this.age)
//     }
// }
// let c1  = new Customer(100,"Ravi",21);
// c1.display();
// short cut constructor initialization 
var Customer = /** @class */ (function () {
    //cid:number;                 // instance variable 
    //cname:string;
    //age:number;
    // in type script we can't write more than one constructor 
    // we can write empty or parameter but only one
    function Customer(cid, cname, age) {
        this.cid = cid;
        this.cname = cname;
        this.age = age;
        //this.cid=cid;
        //this.cname = cname;
        //this.age = age;
    }
    Customer.prototype.display = function () {
        console.log(" id is " + this.cid);
        console.log("name is " + this.cname);
        console.log("age is " + this.age);
    };
    return Customer;
}());
var c1 = new Customer(100, "Ravi", 21);
console.log(c1.age);
c1.display();
var c2 = new Customer(101, "Ramesh");
c2.display();
