// user-defined object creation in ES5 style 
// function Employee() {
//     this.id = 100;
//     this.fname ="Ravi";
//     this.age = 21;
//     this.displayInfo = function() {
//         console.log("id is "+this.id);
//         console.log("name is "+this.fname)
//         console.log("age is "+this.age);
//     }
// }
// var emp1 = new Employee();
// emp1.displayInfo();
//ES5 style 
function Employee() {
    this.id = 100; // instance variable 
    this.fname = "Ravi";
    this.display = function () {
        console.log(" id is " + this.id);
        console.log("name is " + this.fname);
    };
}
var emp1 = new Employee();
emp1.display();
//ES6 style or typescript 
var Customer = /** @class */ (function () {
    function Customer() {
        this.id = 100; // instance variable 
        this.fname = "Ravi";
        console.log("object created...");
    }
    Customer.prototype.display = function () {
        console.log("id is " + this.id);
        console.log("name is " + this.fname);
    };
    return Customer;
}());
var cust1 = new Customer();
cust1.display();
