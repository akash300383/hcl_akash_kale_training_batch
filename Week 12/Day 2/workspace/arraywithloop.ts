let num:Array<number>=[10,20,30,40,50,60];
console.log("classical for loop")
for(let i=0;i<num.length;i++){
    console.log(num[i]);
}

console.log("for in loop")  // it give index position 
for(let i in num){
    console.log("Index position "+i+" Value is "+num[i]);
}

console.log("for of loop")      // it give value 
for(let n of num){
    console.log("Value is "+n)
}

// using forEach function 
num.forEach(n=>console.log(n));