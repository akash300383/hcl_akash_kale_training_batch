// function name and number of parameter must be match 
function add(a,b){
    console.log(a)
    console.log(b);
}
add(10,20);
add(10.20,30.40);
add("a","b");
//add(1);
// function with number of parameter with data types must be match 
// it may return or not return or else it can return anything. 
function empInfo(id:number,name:string,result:boolean){
    console.log(id);
    console.log(name);
    //console.log(result);
}
empInfo(100,"Ravi",true);
// function with it must return number value 
function getNumber(): number {
    return 100;
}
// function with return string value 
function sayHello(): string {
    return "Welcome";
}
// function with no return type 
function info():void {

}
// function with any return type or may not be return 
function hello() : any {

}
