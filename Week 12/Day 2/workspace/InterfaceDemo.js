var Sample = /** @class */ (function () {
    function Sample() {
    }
    Sample.prototype.display = function () {
        console.log("This is interface function");
    };
    return Sample;
}());
var s = new Sample();
s.display();
