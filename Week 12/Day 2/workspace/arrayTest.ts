// array initialization using literal style 
let num:number[]=[10,20,30,40];
let names:string[]=["Ravi","Raj","Ramesh"];
let info:any[]=[1,"Ravi",true,12000.50];

// array initilization using generic style 
let num1:Array<number>=[10,20,30,40,50];
let names1:Array<string>=["Ramesh","RAvi","Lokesh"];
let info1:Array<any>=[1,"Ravi",true,123.50];


// array methods 
let abc:number[]=[];
let xyz:Array<number>=[];
console.log(abc.length)     // size is 0
console.log(xyz.length)     // size is 0
abc.push(10);       // it add the element at the last 
abc.push(20);
abc.push(30);
abc.unshift(100);   // it add the element at the begining 
abc.unshift(200);
abc.unshift(300);
console.log(abc);   // it display as a string. 
abc.pop();          // remove the elements from last 
abc.shift();      // remove the elements from begining 
console.log(abc);

// splice this method is use to add, remove and update array elements in between. 
//abc.splice(1,1);        // from index position 1 it will remove 1 elements.
//abc.splice(1,2);        // from index position 1 it will remove 2 elements. 
//abc.splice(1,0,11);         // add 11 element in 1 position 
abc.splice(1,1,11);             // replace 1 position element by 11 
console.log(abc);






