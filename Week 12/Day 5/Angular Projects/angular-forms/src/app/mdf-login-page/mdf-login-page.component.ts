import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
@Component({
  selector: 'app-mdf-login-page',
  templateUrl: './mdf-login-page.component.html',
  styleUrls: ['./mdf-login-page.component.css']
})
export class MdfLoginPageComponent implements OnInit {
  
  // Validators.required equal to required attribute in html page 
  loginRef = new FormGroup({
    // 1st parameter default value and 
    user:new FormControl("",[Validators.required,Validators.minLength(2)]),          
    pass:new FormControl("",[Validators.required,Validators.pattern("\[a-z]+@[a-z]+.com")])
  });
  msg:string ="";
  constructor() { }

  ngOnInit(): void {
  }

  checkUser(): void {
    let login = this.loginRef.value;
    //console.log(login);
    if(login.user=="Raj" && login.pass=="123"){
        this.msg = "Successfully login"
    }else {
        this.msg = "Failure try once again"
    }
    this.loginRef.reset();
  }

}
